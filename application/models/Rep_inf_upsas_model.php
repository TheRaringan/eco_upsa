<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personas_model
 *
 * @author Nelly Moreno & Albert Torres
 */
class Rep_inf_upsas_model extends CI_Model{

	public function listar_inf_upsas(){
		$consulta_sql= "SELECT
							upsa.id_upsa,
							CONCAT(cor.nombre,' ',cor.apellido) as coordinador,
							ubc.id_ubicacion,
							act_eco.id_actividad_econ_upsa,
							rec.id_recursos_hidr_upsa,
							cap_op.id_capacidad_operativa

						FROM UPSA
						left join ubicacion as ubc ON ubc.id_upsa = UPSA.id_upsa
						LEFT JOIN coordinador as cor ON cor.id_upsa = upsa.id_upsa
						left join actividad_econ_upsa as act_eco ON act_eco.id_upsa = UPSA.id_upsa
						left join recursos_hidr_upsa as rec ON rec.id_upsa = UPSA.id_upsa
						left join vocacion_upsa as voc ON voc.id_upsa = UPSA.id_upsa
						left join capacidad_operativa as cap_op ON cap_op.id_upsa = UPSA.id_upsa

						group by upsa.id_upsa,id_ubicacion,cor.nombre,
							cor.apellido,act_eco.id_actividad_econ_upsa,rec.id_recursos_hidr_upsa,cap_op.id_capacidad_operativa";

		$query = $this->db->query($consulta_sql);

		if($query->result() != ""){
			foreach ($query->result() as $option){
				$data[] = array($option->id_upsa,
								$option->coordinador,
								$option->id_ubicacion,
								$option->id_actividad_econ_upsa,
								$option->id_recursos_hidr_upsa,
								$option->id_capacidad_operativa
							);
			 }
			return $data;
		}else{
			$data = FALSE;
			return $data;
		}
	}
} //<!--clase Personas_model-->
