<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trabajadores_model
 *
 * @author Nelly Moreno
 */
class Trabajadores_model extends CI_Model
{
	
	 public function listar_trabajadores()
    {
					
		/*$this->db->select('t.cedula, 
                            g.nombre as gerencia,
                            c.nombre as cargo, 
                            p.primer_nombre||" "|| p.primer_apellido as aprobador')
                ->from('e_sislogin.trabajadores as t')
                ->join('e_sislogin.gerencias as g', 't.id_gerencia = g.id_gerencia')
                ->join('e_sislogin.cargos as c', 't.id_cargo = c.id_cargo')
                ->join('e_sislogin.personas as p', 't.cedula = p.cedula');
        $query = $this->db->get();*/
        $consulta_sql = "select 
								t.cedula, 
								g.nombre as gerencia,
								c.nombre as cargo, 
								p.primer_nombre||' '|| p.primer_apellido as aprobador
						from
							e_sislogin.trabajadores as t
						inner join
							e_sislogin.gerencias as g
						on
							t.id_gerencia = g.id_gerencia
						inner join
							e_sislogin.cargos as c
						on	
								t.id_cargo = c.id_cargo
						inner join
								e_sislogin.personas as p
						on
							t.cedula = p.cedula";
		$query = $this->db->query($consulta_sql); 
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->cedula, $option->gerencia, $option->cargo, $option->aprobador);
        }
        return $data;
	}
	 public function consultar_persona_method($cedula)
    {
					
		$consulta_sql = "select (primer_nombre || ' ' || primer_apellido) AS Nombre_Completo from e_sislogin.personas where cedula=".$cedula; 
		$query = $this->db->query($consulta_sql); 
         return $query->result();
        //print_r($this->db->last_query());
	}
	 public function cargar_gerencias()
    {
    
        $this->db->select('id_gerencia, nombre')
                ->from('e_sislogin.gerencias');
        $query = $this->db->get();
        
        print_r($this->db->last_query());
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_gerencia, $option->nombre);
        }
        return $data;

    }
    
    public function cargar_aprobadores()
    {
    
        
        $consulta_sql = "select a.cedula, p.primer_nombre||' '||p.primer_apellido as nombre from e_sislogin.aprobadores as a left join e_sislogin.personas as p on a.cedula= p.cedula"; 
		$query = $this->db->query($consulta_sql); 
		 //print_r($this->db->last_query());
        // return $query->result();
        
       
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->cedula, $option->nombre);
        }
        return $data;

    }
    
    public function cargar_indicadores()
    {
    
        $this->db->select('id_indicador, nombre')
                ->from('e_sislogin.indicadores');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_indicador, $option->nombre);
        }
        return $data;

    }
    
    public function cargar_cargos()
    {
    
        $this->db->select('id_cargo, nombre')
                ->from('e_sislogin.cargos');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_cargo, $option->nombre);
        }
        return $data;

    }
     

    
	public function insertar_method($cedula, $id_gerencia, $extension, $indicador, $id_cargo,$id_aprobador){
		
		//echo "ced ".$cedula . "ger " . $id_gerencia . "ext ". $extension . "ind" . $indicador . "carg " . $id_cargo . "apro " . $cedula_aprobador;
		if ($cedula<>0 and $id_gerencia<>0 and $extension<>'' and $indicador<>0 and $id_cargo<>0 and $id_aprobador<>0){
			
			$this->db->select('*')
				->from('e_sislogin.trabajadores')
				->where('cedula', $cedula, TRUE);
			$return = $this->db->get();
			
			//valido que no exista ese numero de cedula
			if ($return->num_rows() >= 1){
				 
				return 1004;  
				    
			   
			}else{
					$cedula = $this->input->post('cedula');
					$id_gerencia = $this->input->post('id_gerencia');
					$extension = $this->input->post('extension');
					$indicador = $this->input->post('indicador');
					$id_cargo = $this->input->post('id_cargo');
					$id_aprobador = $this->input->post('id_aprobador');
							
					$data = array(
							'cedula' => $cedula,
							'id_gerencia' => $id_gerencia,
							'id_extension' => $extension,
							'indicador' => $indicador,
							'id_cargo' => $id_cargo,
							'cedula_aprobador' => $id_aprobador
				  
					);
					
					$result = $this->db->insert('e_sislogin.trabajadores',$data);
					if ($result){
						return 1001;
								
					}else{
						return 1000;
					} //</se inserto>
			}//</que no exista
		}else{
			return 1005;
		}//validacion que no esten vacios*/
	} //<!---insertar_method-->

} //<!--clase Personas_model-->
