<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Agrupaciones_model
 *
 * @author Nelly Moreno
 */
class Agrupaciones_model extends CI_Model
{
	/* $tipo_agrupacion = $this->agrupaciones_model->cargar_tipo_agrupaciones();
        $estado = $this->agrupaciones_model->cargar_estados();
        $cod_area = $this->agrupaciones_model->cargar_cod_area();*/
	
	 public function cargar_tipo_agrupaciones()
    {
    
        $this->db->select('id_tipo_agrupacion, nombre')
                ->from('e_sislogin.tipos_agrupaciones');
        $query = $this->db->get();
        
        //print_r($this->db->last_query());
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_tipo_agrupacion, $option->nombre);
        }
        return $data;

    }
    
    public function consultar_persona_method($cedula)
    {
					
		$consulta_sql = "select (primer_nombre || ' ' || primer_apellido) AS Nombre_Completo from e_sislogin.personas where cedula=".$cedula; 
		$query = $this->db->query($consulta_sql); 
         return $query->result();
        //print_r($this->db->last_query());
	}
    
    public function cargar_estados()
    {
    
        $this->db->select('id_estado, nombre')
                ->from('e_sislogin.estados');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_estado, $option->nombre);
        }
        return $data;

    }
        
	public function insertar_method($nombre_agrupacion,$tipo_agrupacion,$correo,$estado,$cedula_lider,$celular_lider,$cedula_contato_uno,$celular_uno,$cedula_contato_dos,$celular_dos){
		
		//echo "ced ".$cedula . "ger " . $id_gerencia . "ext ". $extension . "ind" . $indicador . "carg " . $id_cargo . "apro " . $cedula_aprobador;
		if ($nombre_agrupacion <>'' and $tipo_agrupacion <> 0  and $correo <>''  and $estado <> 0 and $cedula_lider <> 0 and $celular_lider<>'' ){
			
			$this->db->select('*')
				->from('e_sislogin.agrupacion')
				->where('nombre', $nombre_agrupacion, TRUE);
			$return = $this->db->get();
			
			//valido que no exista ese numero de cedula
			if ($return->num_rows() >= 1){
				 
				return 1004;  
				    
			   
			}else{
									
							if($cedula_contato_uno=='') 
							{ $cedula_contato_uno=0; 
								}
							
							if($cedula_contato_dos=='') 
							{ $cedula_contato_dos=0;
								}		
							
							$data = array(
									'nombre' => $nombre_agrupacion,
									'id_tipo_agrupacion' => $tipo_agrupacion,
									'correo_agrupacion' => $correo,
									'id_estado' => $estado,
									'cedula_lider' => $cedula_lider,
									'celular_lider' => $celular_lider,
									'cedula_contacto_uno' => $cedula_contato_uno,
									'celular_contacto_uno' => $celular_uno,
									'cedula_contacto_dos' => $cedula_contato_dos,
									'celular_contacto_dos' => $celular_dos,
									'activo' => 1
						  
							);
							
							$result = $this->db->insert('e_sislogin.agrupacion',$data);
							
							if ($result){
								return 1001;
										
							}else{
								return 1000;
							} //</se inserto>
			}//</que no exista
		}else{
			return 1005;
		}//validacion que no esten vacios*/
	} //<!---insertar_method-->

} //<!--clase Personas_model-->


