<?php
/**
 * @author Albert Torres
 */
class Vocacion_model extends CI_Model{
	public function cargar_vocacion(){
		$this->db->select('id_sistema_riego, nombre');
		$this->db->order_by('nombre');
		$query = $this->db->get('sistema_riego');
		return $query->result_array();
	}
	
	public function insertar_vocacion($i,$id_upsa,$clase){
		$query = $this->db->query(	"SELECT insertar_vocacion_upsa(".$id_upsa.",".$clase[$i].")");
		return $query->result();
	}
	
	public function consultar_registro_vocacion(){
		$this->db->where('id_upsa',$this->session->userdata('id_upsa'));
		$query = $this->db->get('vocacion_upsa');
		return $query->result_array();
	}
}