<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personas_model
 *
 * @author Nelly Moreno
 */
class Personas_model extends CI_Model
{
	 public function cargar_nacionalidades()
    {
    
        $this->db->select('id_nacionalidad, nombre')
                ->from('e_sislogin.nacionalidades');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_nacionalidad, $option->nombre);
        }
        return $data;

    }
    
    public function consultar_persona_method($cedula)
    {
					
		$consulta_sql = "select id_persona, 
								cedula, 
								primer_nombre, 
								segundo_nombre, 
								primer_apellido, 
       							segundo_apellido, 
       							id_nacionalidad 
       					from 
       							e_sislogin.personas
       					 where 
       					 		cedula=$cedula"; 

		$query = $this->db->query($consulta_sql); 
         return $query->result();
        //print_r($this->db->last_query());
	}

	public function actualizar_method($cedula, $primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido,$nacionalidad){
		$data = array(
            'primer_nombre' => $primer_nombre,
            'segundo_nombre' => $segundo_nombre,
            'primer_apellido' => $primer_apellido,
            'segundo_apellido' => $segundo_apellido,
            'id_nacionalidad' => $nacionalidad
        );
        $this->db->where('cedula', $cedula);
        return $this->db->update('e_sislogin.personas', $data);

	}
     
    
	public function insertar_method($cedula, $primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido,$nacionalidad){
		
			
		if ($cedula<>0 and $primer_nombre<>'' and $primer_apellido<>'' and $nacionalidad<>0){
			
			$this->db->select('*')
				->from('e_sislogin.personas')
				->where('cedula', $cedula, TRUE);
			$return = $this->db->get();
			
			//valido que no exista ese numero de cedula
			if ($return->num_rows() >= 1){
				 
				return 1004;  
				    
			   
			}else{
					$cedula = $this->input->post('cedula');
					$primer_nombre = $this->input->post('primer_nombre');
					$segundo_nombre = $this->input->post('segundo_nombre');
					$primer_apellido = $this->input->post('primer_apellido');
					$segundo_apellido = $this->input->post('segundo_apellido');
					$nacionalidad = $this->input->post('nacionalidad');
							
					$data = array(
							'cedula' => $cedula,
							'primer_nombre' => $primer_nombre,
							'segundo_nombre' => $segundo_nombre,
							'primer_apellido' => $primer_apellido,
							'segundo_apellido' => $segundo_apellido,
							'id_nacionalidad' => $nacionalidad
				  
					);
					
					$result = $this->db->insert('e_sislogin.personas',$data);
					if ($result){
						return 1001;
								
					}else{
						return 1000;
					} //</se inserto>
			}//</que no exista
		}else{
			return 1005;
		}//validacion que no esten vacios*/
	} //<!---insertar_method-->

	public function listar_personas()
    {
        $this->db->select('p.id_persona as cod, 
                            p.cedula,
                             p.primer_nombre,
                              p.segundo_nombre,
                               p.primer_apellido,
                                p.segundo_apellido, 
                            	n.nombre as nacionalidad')
                ->from('e_sislogin.personas as p')
                ->join('e_sislogin.nacionalidades as n', 'p.id_nacionalidad = n.id_nacionalidad');
        $query = $this->db->get();
        
        foreach ($query->result() as $option)
        {
            $data[] = array($option->cod, $option->cedula, $option->primer_nombre, $option->segundo_nombre, $option->primer_apellido, $option->segundo_apellido, $option->nacionalidad);
        }
        return $data;
    }

} //<!--clase Personas_model-->
