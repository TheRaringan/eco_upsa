<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of listar_ervicios_model
 *
 * @author Nelly Moreno
 */
class listar_servicios_model extends CI_Model
{
	
       
     public function listar_servicios()
    {
		$consulta_sql = "SELECT 
								s.id_solicitud_servicio as codigo, 
								t.nombre as tipo_solicitud,
								s.cedula_solicitante,
								p.primer_nombre||' '||p.primer_apellido as nombre_solicitante, 
								s.titulo_evento, 
								o.nombre as indicador, 
								pe.primer_nombre||' '||pe.primer_apellido as aprobador, 
								e.nombre as estatus
							  FROM 
								e_sislogin.solicitudes_servicios as s
							INNER JOIN
								e_sislogin.personas as p
							ON
								s.cedula_solicitante = p.cedula
							
							INNER JOIN
								e_sislogin.tipos_solicitudes as t
							ON
								s.id_tipo_solicitud = t.id_tipo_solicitud
							INNER JOIN
								e_sislogin.indicadores as o
							on 
								s.orden_interna = o.id_indicador
						
							INNER JOIN
								e_sislogin.personas as pe
							ON
								s.cedula_aprobador = pe.cedula
							INNER JOIN
								e_sislogin.estatus as e
							ON
								s.id_estatus_servicio = e.id_estatus;"; 
		$query = $this->db->query($consulta_sql); 
		//echo $this->db->last_query();
        if($query ){
	        foreach ($query->result() as $option)
	        {
	            $data[] = array($option->codigo, 
								$option->tipo_solicitud, 
								$option->cedula_solicitante, 
								$option->nombre_solicitante,
								$option->titulo_evento,
								$option->indicador,
								$option->aprobador,
								$option->estatus);
	        }
    	}else {
    		$data[] ="No hay Registros ";
    	}
        return $data;

    }
}
