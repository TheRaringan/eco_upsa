<?php

	class Select_recursos_model extends CI_Model{
		public function get_tipos_recursos(){
			$this->db->select('id_tipo_recurso_hidr,nombre');
			$this->db->order_by('nombre');
			$query = $this->db->get('tipo_recurso_hidr');
			return $query->result_array();
		}

		public function get_recursos_hidricos($postData){
			$this->db->select('id_recurso_hidr,nombre');
			$this->db->where('id_tipo_recurso_hidr',$postData['this_tipo_recurso']);
			$this->db->order_by('nombre');
			$query = $this->db->get('recurso_hidr');
			$response = $query->result_array();
			return $response;
		}
	}
