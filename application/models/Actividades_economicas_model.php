<?php

	class Actividades_economicas_model extends CI_Model{
		public function insertar_actividades(	$i,
												$id_upsa,
												$id_tipo_actividad_econ,
												$id_actividad_econ,
												$id_rubro,
												//$id_subrubro,
												$produccion_mensual,
												$id_unidad_medida,
												$id_estado_operacion,
												$id_laboral,
												$id_electricidad,
												$id_mecanico,
												$id_seguridad,
												$id_climatico,
												$id_materia_prima,
												$observaciones
											){
			$query= $this->db->query(	"SELECT insertar_actividad_econ_upsa(".
												$id_upsa.",".
												$id_tipo_actividad_econ[$i].",".
												$id_actividad_econ[$i].",".
												$id_rubro[$i].",".
												//$id_subrubro[$i].",".
												$produccion_mensual[$i].",".
												$id_unidad_medida[$i].",".
												$id_estado_operacion[$i].",".
												$id_laboral.",".
												$id_electricidad.",".
												$id_mecanico.",".
												$id_seguridad.",".
												$id_climatico.",".
												$id_materia_prima.",'".
												$observaciones."')");
			return $query->result();
		}

		public function listar_act_prod($id_upsa){
			$consulta_sql="select u.id_upsa,
								  tip_e.nombre as tipo_act_econ,
								  ace.nombre as act_econ,
								  r.nombre as rubro,
								  act.produccion_mensual,
								  um.nombre as unidad_medida,
								  es.nombre as estado_operacion
						from actividad_econ_upsa as act
						       LEFT JOIN upsa as u ON u.id_upsa = act.id_upsa
						       LEFT JOIN tipo_actividad_econ as tip_e ON tip_e.id_tipo_actividad_econ = act.id_tipo_actividad_econ
						       LEFT JOIN actividad_econ as ace ON ace.id_actividad_econ = act.id_actividad_econ
						       LEFT JOIN rubro as r ON r.id_rubro = act.id_rubro
						       LEFT JOIN unidad_medida as um ON um.id_unidad_medida = act.id_unidad_medida
						       LEFT JOIN estado_operacion as es ON es.id_estado_operacion = act.id_estado_operacion
						where u.id_upsa = '".$id_upsa."'";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->id_upsa,
													$key->tipo_act_econ,
													$key->act_econ,
													$key->rubro,
													$key->produccion_mensual,
													$key->unidad_medida,
													$key->estado_operacion
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}
	}
