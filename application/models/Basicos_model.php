<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Personas_model
 *
 * @author Nelly Moreno
 */
class Basicos_model extends CI_Model
{
	public function insertar_method($nombre_upsa, $cod_sunagro, $cod_insai, $id_agropatria, $id_tenencia_tierra, $id_relacion_corpo, $cedula, $nombre, $apellido, $tlf_local, $tlf_movil, $email){

		if ($nombre_upsa<>'' and $cod_insai<>0){

			$this->db->select('*')
				->from('upsa')
				->where('nombre', $nombre_upsa, TRUE);
			$return = $this->db->get();

			//valido que no exista ese numero de cedula
			if ($return->num_rows() >= 1){

				return 1004;

			}else{
					$nombre_upsa = $this->input->post('nombre_upsa');
					$cod_sunagro = $this->input->post('cod_sunagro');
					$cod_insai = $this->input->post('cod_insai');
					$id_agropatria = $this->input->post('id_agropatria');
					$id_tenencia_tierra = $this->input->post('id_tenencia_tierra');
					$id_relacion_corpo = $this->input->post('id_relacion_corpo');
					$cedula = $this->input->post('cedula');
					$nombre = $this->input->post('nombre');
					$apellido = $this->input->post('apellido');
					$tlf_local = $this->input->post('tlf_local');
					$tlf_movil = $this->input->post('tlf_movil');
					$email = $this->input->post('email');
					$id_usuario = $this->session->userdata('id_usuario');

					$consulta_sql = "SELECT insertar_upsa(
									       					'$nombre_upsa',
																	$cod_sunagro,
																	$cod_insai,
																	$id_agropatria,
																	$id_tenencia_tierra,
																	$id_relacion_corpo,
																	$cedula,
																	'$nombre',
																	'$apellido',
																	'$tlf_local',
																	'$tlf_movil',
																	'$email',
																	'$id_usuario'
														);";
					$result = $this->db->query($consulta_sql);
					//echo $this->db->last_query();
					if ($result){
						$this->session->set_userdata('id_upsa',$result->row()->insertar_upsa);
						$this->session->set_userdata('nombre_upsa',$nombre_upsa);
						return 1001;
					}else{
						return 1000;
					} //</se inserto>
			}//</que no exista
		}else{
			return 1005;
		}//validacion que no esten vacios*/
	} //<!---insertar_method-->

	public function insertar_basicos( $nombre_upsa,
																			$cod_sunagro,
																			$cod_insai,
																			$id_agropatria,
																			$id_tenencia_tierra,
																			$id_relacion_corpo,
																			$cedula,
																			$nombre_coordinador,
																			$apellido_coordinador,
																			$tlf_local,
																			$tlf_movil,
																			$email){

		$this->db->where('nombre',$nombre_upsa);
		$query = $this->db->get('upsa');
		if($query->num_rows() == 1){
			return FALSE;
		}
		
		$this->db->trans_start();
		$query= $this->db->query(	"SELECT insertar_upsa(
																										'$nombre_upsa',
																										$cod_sunagro,
																										$cod_insai,
																										$id_agropatria,
																										$id_tenencia_tierra,
																										$id_relacion_corpo,
																										$cedula,
																										'$nombre',
																										'$apellido',
																										'$tlf_local',
																										'$tlf_movil',
																										'$email'
																										)");
		$this->db->trans_complete();
		return $query->result();
	}


	 public function consultar_agropatria()
    {

        $this->db->select('id_agropatria, nombre')
                ->from('agropatria');
        $query = $this->db->get();
        //echo $this->db->last_query();

        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_agropatria, $option->nombre);
        }
        return $data;



    }

     public function consultar_tenencia()
    {

        $this->db->select('id_tenencia_tierra, nombre')
                ->from('tenencia_tierra');
        $query = $this->db->get();
        //echo $this->db->last_query();

        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_tenencia_tierra, $option->nombre);
        }
        //var_dump ($data);
        return $data;



    }

     public function consultar_relacion_corpo()
    {

        $this->db->select('id_relacion_corpo, nombre')
                ->from('relacion_corpo');
        $query = $this->db->get();
        //echo $this->db->last_query();

        foreach ($query->result() as $option)
        {
            $data[] = array($option->id_relacion_corpo, $option->nombre);
        }
        return $data;
		}
		
		public function consultar_registro_upsa(){

			$this->db->select(	'upsa.nombre as nombre_upsa,
													upsa.cod_sunagro as cod_sunagro,
													upsa.cod_insai as cod_insai,
													upsa.id_agropatria as id_agropatria,
													upsa.id_tenencia_tierra as id_tenencia_tierra,
													upsa.id_relacion_corpo as id_relacion_corpo,
													coordinador.nombre as nombre_coordinador,
													coordinador.apellido as apellido_coordinador,
													coordinador.cedula as cedula,
													coordinador.tlf_local as tlf_local,
													coordinador.tlf_movil as tlf_movil,
													coordinador.email as email'
												);
			$this->db->join('upsa','coordinador.id_upsa = upsa.id_upsa');
			$this->db->where('coordinador.id_upsa',$this->session->userdata('id_upsa'));
			$query = $this->db->get('coordinador');
			if($query->num_rows() == 1){
				return $query->row_array();
			}else{
				return FALSE;
			}						
		}








} //<!--clase Carga de UPSA-->
