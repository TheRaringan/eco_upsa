<?php
	class Rep_cuadros_upsas_model extends CI_Model{

		public function listar_op(){
			$consulta_sql="SELECT es.descripcion as estado,
								  upsa.nombre as nombre_upsa,
								  est_op.nombre as operatividad
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN actividad_econ_upsa as eco ON eco.id_upsa = upsa.id_upsa
							INNER JOIN estado_operacion as est_op ON est_op.id_estado_operacion = eco.id_estado_operacion";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->operatividad
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function listar_act_prod(){
			$consulta_sql="SELECT es.descripcion as estado,
								  upsa.nombre as nombre_upsa,
								  tip_act.nombre as actividad
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN actividad_econ_upsa as eco ON eco.id_upsa = upsa.id_upsa
							INNER JOIN tipo_actividad_econ as tip_act ON tip_act.id_tipo_actividad_econ = eco.id_tipo_actividad_econ";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->actividad
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function listar_rec_hib(){
			$consulta_sql="SELECT es.descripcion as estado,
								  upsa.nombre as nombre_upsa,
								  tip_rec_hib.nombre as tipo_recurso,
								  rec.nombre as recurso
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN recursos_hidr_upsa as eco ON eco.id_upsa = upsa.id_upsa
							INNER JOIN tipo_recurso_hidr as tip_rec_hib ON tip_rec_hib.id_tipo_recurso_hidr = eco.id_tipo_recurso_hidr
							INNER JOIN recurso_hidr as rec ON  rec.id_recurso_hidr = eco.id_recurso_hidr";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->tipo_recurso,
													$key->recurso
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

		public function listar_voc_tie(){
			$consulta_sql="SELECT es.descripcion as estado,
								  upsa.nombre as nombre_upsa,
								  uso_voc. nombre as uso,
								  voc.clase,
								  voc.rubro as tipo_recurso
							FROM upsa
							LEFT JOIN ubicacion as ub ON ub.id_upsa = upsa.id_upsa
							LEFT JOIN estado as es ON es.id_estado = ub.id_estado
							LEFT JOIN vocacion_upsa as voc_upsa ON voc_upsa.id_upsa = upsa.id_upsa
							INNER JOIN vocacion as voc ON voc.id_vocacion = voc_upsa.id_vocacion
							INNER JOIN uso_vocacion AS uso_voc ON uso_voc.id_uso_vocacion = voc.id_uso_vocacion";

							$query =  $this->db->query($consulta_sql);

							if ($query->result() != NULL) {
								foreach ($query->result() as $key) {
									$data[] = array($key->estado,
													$key->nombre_upsa,
													$key->uso,
													$key->clase,
													$key->tipo_recurso
												);
								}
								return $data;
							}else {
								$data = FALSE;
								return $data;
							}
		}

	}

?>
