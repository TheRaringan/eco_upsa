<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of listar_auditoria_model
 *
 * @author Nelly Moreno
 */
class listar_auditoria_model extends CI_Model
{
	
       
     public function listar_auditoria()
    {
		$this->db->select('*');								
		$this->db->from('auditoria');
		
		$query = $this->db->get();  
		
		//echo $this->db->last_query();
		         
        foreach ($query->result() as $option)
        {
            $data[] = array( $option->id_aud, $option->seccion, $option->accion, $option->campoclave, $option->id_us, $option->ip, $option->fecha, $option->hora);
        }

         
        return $data;
    }
}
