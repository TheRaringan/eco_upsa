<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login_model
 *
 * @author Nelly Moreno
 */
class Usuarios_model extends CI_Model
{
	/*  public function consulta_login($usuario, $clave)
    {
			$usuario = $this->input->post('usuario');
			$clave = $this->input->post('clave');
		   $this -> db -> select('usuario, clave, perfil_id');
		   $this -> db -> from('maestro.usuarios');
		   $this -> db -> where('usuario', $usuario);
		   $this -> db -> where('clave', MD5($clave));
		   $this -> db -> limit(1);

		   $query = $this -> db -> get();
		   //echo $this->db->last_query();
		   //die;

		if($query -> num_rows() == 1)
	   {
		 return $query->result();
	   }
	   else
	   {

		 return false;
	   }

		} */

		public function login($usuario, $clave){

			$this->db->select(	'maestro.usuarios.id_usuario,
								maestro.usuarios.usuario,
								maestro.usuarios.clave,
								maestro.usuarios.perfil_id,
								maestro.usuarios.id_upsa,
								public.upsa.nombre'
			);

			$this->db->where('usuario',$usuario);
			$this->db->from('maestro.usuarios');
			$this->db->join('public.upsa', 'public.upsa.id_upsa = maestro.usuarios.id_upsa', 'left');
			$result = $this->db->get();

			//print_r($result->result_array());die;


			if($result->num_rows() == 1){
				$db_clave = $result->row('clave');
				if(password_verify( base64_encode(
					hash('sha256', $clave, true)
				),$db_clave)){
					return $result->row_array();
				}
			}else{
				return FALSE;
			}
		}

		public function cargar_upsa(){
			$this->db->select('id_upsa,nombre');
			$this->db->order_by('nombre');
			$query = $this->db->get('upsa');

			return $query->result_array();
		}

		public function registrar_usuario($data){
			return $this->db->insert('maestro.usuarios',$data);
		}

		public function cambiar_contrasenia($data){

			$campos="UPDATE maestro.usuarios
					SET clave='".$data["clave"]."'
					WHERE usuario = '".$data["usuario"]."'	";
			//echo ($campos);die;
		    $tabla=$this->db->query($campos);
		    return $tabla;
			//print_r ($data);die;
			// $this->db->update('maestro.usuarios');
			// $this->db->where('usuario','".$data["usuario"]."');
			// $result = $this->db->get();

		}
} //<!--clase Login_model-->
