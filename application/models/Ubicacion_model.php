<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login_model
 *
 * @author Nelly Moreno
 */
	class Ubicacion_model extends CI_Model{

			public function get_region(){
				$this->db->select('id_region, descripcion');
				$this->db->order_by('descripcion');
				$query = $this->db->get('region');
				return $query->result_array();
			}

			public function get_estado($postData){
				$response = array();
				$this->db->select('estado.id_estado,estado.descripcion');
				$this->db->where('estado.id_region', $postData['this_id_region']);
				$q = $this->db->get('estado');
				$response = $q->result_array();
				return $response;
			}

			public	function get_municipio($postData){
				$response = array();
				$this->db->select('municipio.id_municipio,municipio.descripcion');
				$this->db->where('municipio.id_estado', $postData['this_id_estado']);
				$this->db->order_by('municipio.descripcion');
				$q = $this->db->get('municipio');
				$response = $q->result_array();
				return $response;
			}

			public	function get_parroquia($postData){
				$response = array();
				$this->db->select('id_parroquia,descripcion');
				$this->db->where('id_municipio', $postData['this_id_municipio']);
				$this->db->order_by('descripcion');
				$q = $this->db->get('parroquia');
				$response = $q->result_array();
				return $response;
			}
			
			public function insertar_ubicacion( $id_region,
																					$id_estado,
																					$id_municipio,
																					$id_parroquia,
																					$cod_upsa,
																					$id_upsa,
																					$direccion,
																					$lindero_norte,
																					$lindero_sur,
																					$lindero_este,
																					$lindero_oeste){

				$this->db->trans_start();
				$query= $this->db->query(	"SELECT insertar_ubicacion(
													 										       				$id_region,
													 															    $id_estado,
													 															    $id_municipio,
													 															    $id_parroquia,
													 															    '$cod_upsa',
																														 $id_upsa,
													 															    '$direccion',
													 															    '$lindero_norte',
													 															    '$lindero_sur',
													 															    '$lindero_este',
													 															    '$lindero_oeste')");
				$this->db->trans_complete();
				return $query->result();

			}

			public function chequeando_ubicacion(){
				$this->db->where('id_upsa',$this->session->userdata('id_upsa'));
				$query = $this->db->get('ubicacion');
				if($query->num_rows() == 1){
					return TRUE;
				}else{
					return FALSE;
				}
			}

			public function consultar_registro_ubicacion(){

				$this->db->select(	'region.descripcion as region,
														estado.descripcion as estado,
														municipio.descripcion as municipio,
														parroquia.descripcion as parroquia,
														ubicacion.cod_upsa as cod_upsa,
														ubicacion.direccion as direccion,
														ubicacion.lindero_norte as lindero_norte,
														ubicacion.lindero_sur as lindero_sur,
														ubicacion.lindero_este as lindero_este,
														ubicacion.lindero_oeste as lindero_oeste'
													);
				$this->db->join('region','ubicacion.id_region = region.id_region');
				$this->db->join('estado','ubicacion.id_estado = estado.id_estado');
				$this->db->join('municipio','ubicacion.id_municipio = municipio.id_municipio');
				$this->db->join('parroquia','ubicacion.id_parroquia = parroquia.id_parroquia');
				$this->db->join('upsa','ubicacion.id_upsa = upsa.id_upsa');
				$this->db->where('ubicacion.id_upsa',$this->session->userdata('id_upsa'));
				$query = $this->db->get('ubicacion');
				if($query->num_rows() == 1){
					return $query->row_array();
				}else{
					return FALSE;
				}						
			}

		// public function insertar_method($id_region,
		// 																$id_estado,
		// 																$id_municipio,
		// 																$id_parroquia,
		// 																$cod_upsa,
		// 																$direccion,
		// 																$lindero_norte,
		// 																$lindero_sur,
		// 																$lindero_este,
		// 																$lindero_oeste)
		// 										{
		// 										if ($id_region<>0 and
		// 										    $id_estado<>0 and
		// 												$id_municipio<>0 and
		// 												$id_parroquia<>0 and
		// 												$cod_upsa<>'' and
		// 												$direccion<>'' and
		// 												$lindero_norte<>'' and
		// 												$lindero_sur<>'' and
		// 												$lindero_este<>'' and
		// 												$lindero_oeste<>''){

		// 	$this->db->select('*')
		// 		       ->from('ubicacion');
		// 		       //->where('id_upsa', $id_upsa, TRUE);
		// 	$return = $this->db->get();


		// 	//valido que no exista ese numero de cedula
		// 	if ($return->num_rows() >= 1){
		// 		return 1004;
		// 	}else{
		// 			$id_region     = $this->input->post('id_region');
		// 			$id_estado     = $this->input->post('id_estado');
		// 			$id_municipio  = $this->input->post('id_municipio');
		// 			$id_parroquia  = $this->input->post('id_parroquia');
		// 			$cod_upsa      = $this->input->post('cod_upsa');
		// 			$direccion     = $this->input->post('direccion');
		// 			$lindero_norte = $this->input->post('lindero_norte');
		// 			$lindero_sur   = $this->input->post('lindero_sur');
		// 			$lindero_este  = $this->input->post('lindero_este');
		// 			$lindero_oeste = $this->input->post('lindero_oeste');


					// $consulta_sql = "SELECT insertar_ubicacion(
					// 										       				$id_region,
					// 															    $id_estado,
					// 															    $id_municipio,
					// 															    $id_parroquia,
					// 															    '$cod_upsa',
					// 															    '$direccion',
					// 															    '$lindero_norte',
					// 															    '$lindero_sur',
					// 															    '$lindero_este',
					// 															    '$lindero_oeste');";

		// 			$result = $this->db->query($consulta_sql);
		// 					//echo $this->db->last_query();
		// 						if ($result){
		// 							return 1001;
		// 						}else{
		// 							return 1000;
		// 						} //</se inserto>
		// 				}//</que no exista
		// 			}else{
		// 				return 1005;
		// 			}//validacion que no esten vacios*/
		// 		} //<!---insertar_method-->

	} //<!--clase Login_model-->
