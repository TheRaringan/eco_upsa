<?php
    class Mapa_model extends CI_Model{
        public function get_totales_pecua(){
            $query = $this->db->get('total_upsa_pecua_por_edo');
            return $query->row_array();
        }
        public function get_totales_agric(){
            $query = $this->db->get('total_upsa_agric_por_edo');
            return $query->row_array();
        }
        public function get_total_upsa(){
            $query = $this->db->get('total_upsa_por_edo');
            return $query->row_array();
        }
        public function get_percent_agric(){
            $query = $this->db->get('porc_superf_opert_upsa_agric_edo');
            return $query->row_array();
        }
        public function get_percent_pecua(){
            $query = $this->db->get('porc_superf_opert_upsa_pecua_edo');
            return $query->row_array();
        }
    }
?>