<?php
  class Upsas_localizacion_model extends CI_Model{

    public function get_upsas_coords(){
      $this->db->select('*');
      $query = $this->db->get('upsas_localizacion');
      return $query->result_array();
    }

    public function getUpsas(){
      $this->db->select('*');
      $query = $this->db->get('upsa');
      return $query->result_array();
    }

    public function checkUpsa(){
      $upsa = $this->input->post('upsa');
      $this->db->select('*');
      $this->db->where('id_upsa',13);
      $q = $this->db->get('upsas_coords');
      return $this->db->affected_rows();
    }

    public function updateCoords(){
      $data = array(
				'longitud' => $this->input->post('longitud'),
				'latitud' => $this->input->post('latitud')
			 );
		  $this->db->where('id_upsa', $this->input->post('upsa'));
		  return $this->db->update('upsas_coords' ,$data);
    }

    public function insertUpsaCoords(){
      $data = array(
        'longitud' => floatval($this->input->post('longitud')),
        'latitud' => floatval($this->input->post('latitud')),
        'id_upsa' => $this->input->post('upsa')
      );

      return $this->db->insert('upsas_coords', $data);
    }

    public function get_upsa_agric_edo(){
      $this->db->select('*');
      $query = $this->db->get('porc_superf_opert_upsa_agric_edo');
      return $query->result_array();
    }
  }
?>