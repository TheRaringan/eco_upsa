<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Aprobar_model
 *
 * @author Nelly Moreno
 */
class Aprobar_model extends CI_Model
{
	 
    
    public function consultar_solicitud_method($cedula)
    {
					
		$consulta_sql = "select 
								s.id_solicitud_servicio as cedula, 
								t.nombre as tipo_solicitud, 
								s.fecha as fecha, 
								p.primer_apellido as solicitante, 
								s.titulo_evento as evento, 
       							p.primer_apellido as aprobador
       					from 
       							e_sislogin.solicitudes_servicios as s
       							
       					inner join
								e_sislogin.tipos_solicitudes as t
						on 
								s.id_tipo_solicitud  = t.id_tipo_solicitud
						inner join
								e_sislogin.personas as p
						on 
								s.cedula_solicitante  = p.cedula
       					 where 
       					 		id_solicitud_servicio=$cedula"; 
       					 		

		$query = $this->db->query($consulta_sql); 
         return $query->result();
        //print_r($this->db->last_query());
	}

	public function actualizar_method($boton, $cedula){
		/*$data = array(
            'primer_nombre' => $primer_nombre,
            'segundo_nombre' => $segundo_nombre,
            'primer_apellido' => $primer_apellido,
            'segundo_apellido' => $segundo_apellido,
            'id_nacionalidad' => $nacionalidad
        );
        $this->db->where('cedula', $cedula);
        return $this->db->update('e_sislogin.personas', $data);*/
        
        if ($boton=="Aprobar"){
			
			/*$consulta_sql = "UPDATE e_sislogin.solicitudes_servicios SET id_estatus_servicio = 2 WHERE id_solicitud_servicio = $cedula"; 

			$query = $this->db->query($consulta_sql); 
			 return $query->result();
			 //print_r($this->db->last_query());*/
			 $this->db->set('id_estatus_servicio', 2); 
			 $this->db->where('id_solicitud_servicio', $cedula); 
				$this->db->update('e_sislogin.solicitudes_servicios');
				return 1004;  
			 
        } 
        
        if ($boton=="Rechazar"){
			
			/*$consulta_sql = "UPDATE e_sislogin.solicitudes_servicios SET id_estatus_servicio = 2 WHERE id_solicitud_servicio = $cedula"; 

			$query = $this->db->query($consulta_sql); 
			 return $query->result();
			 //print_r($this->db->last_query());*/
			 $this->db->set('id_estatus_servicio', 6); 
			 $this->db->where('id_solicitud_servicio', $cedula); 
				$this->db->update('e_sislogin.solicitudes_servicios');
				return 1004;  
			 
        } 

	}
     
    
	

	

} //<!--clase Aprobar_model-->
