<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sislogin Pagina principal del Sistema
 *
 * @author Nelly Moreno
 */
class Ubicacion extends CI_Controller
{
  Public $pnoti;

  public function __construct()
   {
       parent::__construct();
       $this->load->model('ubicacion_model', '', TRUE);
       $this->load->helper(array('url', 'form'));
           $this->pnoti=0;
   }

    public function index()
    {
      if(!$this->session->userdata('logged_in')){
        redirect('');
      }
      if($this->session->userdata('id_upsa') == ''){
				redirect('basicos');
      }

      $chequear = $this->ubicacion_model->chequeando_ubicacion();
      $data = array('titulo' => 'UBICACIÓN DE LA UPSA');
      $data['regiones'] = $this->ubicacion_model->get_region();
      //$data['estados'] = $this->ubicacion_model->get_estado();
      $this->load->view('templates/header');
      $this->load->view('templates/navegator');
      if($chequear){
        $this->load->view('ubicacion/registrada', $data);
      }else{
        $this->load->view('ubicacion/index', $data);
      }
      $this->load->view('templates/footer');
    }

    public function get_estado(){
      $postData = $this->input->post();
      $data = $this->ubicacion_model->get_estado($postData);
      echo json_encode($data);
  }

    public function get_municipio(){
      $postData = $this->input->post();
      $data = $this->ubicacion_model->get_municipio($postData);
      echo json_encode($data);
    }

    public function get_parroquia(){
      $postData = $this->input->post();
      $data = $this->ubicacion_model->get_parroquia($postData);
      echo json_encode($data);
    }

    public function insertar_ubicacion(){
      if(!$this->session->userdata('logged_in')){
        redirect('');
      }
      if($this->session->userdata('id_upsa') == ''){
        redirect('basicos');
      }
      $id_upsa = $this->session->userdata('id_upsa');
      $id_region = $this->input->post('id_region');
			$id_estado = $this->input->post('id_estado');
			$id_municipio = $this->input->post('id_municipio');
      $id_parroquia = $this->input->post('id_parroquia');
      $cod_upsa = $this->input->post('cod_upsa');
      if($cod_upsa == ''){
        $cod_upsa = 'N/A';
      }
      $direccion = $this->input->post('direccion');
      if($direccion == ''){
        $direccion = 'N/A';
      }
      $lindero_norte = $this->input->post('lindero_norte');
      if($lindero_norte == ''){
        $lindero_norte = 'N/A';
      }
      $lindero_sur = $this->input->post('lindero_sur');
      if($lindero_sur == ''){
        $lindero_sur = 'N/A';
      }
      $lindero_este = $this->input->post('lindero_este');
      if($lindero_este == ''){
        $lindero_este = 'N/A';
      }
      $lindero_oeste = $this->input->post('lindero_oeste');
      if($lindero_oeste == ''){
        $lindero_oeste = 'N/A';
      }

      $this->ubicacion_model->insertar_ubicacion(
                                                  $id_region,
                                                  $id_estado,
                                                  $id_municipio,
                                                  $id_parroquia,
                                                  $cod_upsa,
                                                  $id_upsa,
                                                  $direccion,
                                                  $lindero_norte,
                                                  $lindero_sur,
                                                  $lindero_este,
                                                  $lindero_oeste
      );
      $this->session->set_flashdata('pnotify','insert');
			redirect('ubicacion');
    }

    // public function procesardatos() {
    //   $boton = $this->input->post('boton');
    //     if($boton=='Guardar'){
      //$this->input->post('id_region'),
      //                             $this->input->post('id_estado'),
      //                             $this->input->post('id_municipio'),
      //                             $this->input->post('id_parroquia'),
      //                             $this->input->post('cod_upsa'),
      //                             $this->input->post('direccion'),
      //                             $this->input->post('lindero_norte'),
      //                             $this->input->post('lindero_sur'),
      //                             $this->input->post('lindero_este'),
      //                             $this->input->post('lindero_oeste'));
    //     $datos = $this->ubicacion_model->insertar_method(
    //                             $this->input->post('id_region'),
    //                             $this->input->post('id_estado'),
    //                             $this->input->post('id_municipio'),
    //                             $this->input->post('id_parroquia'),
    //                             $this->input->post('cod_upsa'),
    //                             $this->input->post('direccion'),
    //                             $this->input->post('lindero_norte'),
    //                             $this->input->post('lindero_sur'),
    //                             $this->input->post('lindero_este'),
    //                             $this->input->post('lindero_oeste'));

    //         //$usuario=$_SESSION['usuario'];
    //    //$auditoria=new auditoria_model();
    //    // $rs=$auditoria->registrar_auditoria("Inicio","Registro de Nueva Persona (usuario:".$usuario.")");

    //             // if($datos == 1001){
    //             //     $this->pnoti=1;
    //             // }else if($datos == 1000)    {
    //             //      $this->pnoti=2;
    //             // }else if($datos == 1004){
    //             //      $this->pnoti=4;
    //             // }else if($datos == 1005){
    //             //      $this->pnoti=5;
    //             // }
    //             //  $this->index();
    //       }
    //     }

}
