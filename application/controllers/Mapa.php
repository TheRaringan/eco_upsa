<?php
    class Mapa extends CI_Controller{
        public function index(){
            if(!$this->session->userdata('logged_in')){
                redirect('');
            }
            $data['title'] = 'Estadisticas UPSA Venezuela';
            $this->load->model('Mapa_model','MM',TRUE);//llamar al modelo
            $data['totales_pecua'] = $this->MM->get_totales_pecua();//asignar a una vaiable el resultado de la funcion del modelo
            $data['totales_agric'] = $this->MM->get_totales_agric();
            $data['total_upsa'] = $this->MM->get_total_upsa();
            $data['total_p_pecua'] = $this->MM->get_percent_pecua();
            $data['totale_p_agric'] = $this->MM->get_percent_agric();
            
            $this->load->view('templates/header');
            $this->load->view('templates/navegator');
            $this->load->view('mapa/index',$data);
            $this->load->view('templates/footer');
        }
}
?>