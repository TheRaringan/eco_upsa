<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author Nelly Moreno
 */
class Usuarios extends CI_Controller{

    public $pnoti;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('usuarios_model','', true);
        $this->load->helper(array('url', 'form'));
        $this->pnoti = 0;
    }

    public function index()
    {
        if($this->session->userdata('logged_in')){
            redirect('principal');
        }
        $data = array('titulo' => 'ECOPRO-UPSA');
        $this->load->view('templates/header', $data);
        //$this->load->view('notificaion');
        //¿$data = array('inicio' => 'INICIAR SESI&Oacute;N');
        $this->load->view('inicio/index', $data);

    }

    public function registro(){
        $data['title'] = 'Registro de nuevo Usuario';
        //$data['upsas'] = $this->usuarios_model->cargar_upsa();

        $this->load->view('templates/header');
        $this->load->view('templates/navegator');
        $this->load->view('usuarios/registro',$data);
        $this->load->view('templates/footer');

    }

    public function cambio_contrasenia(){
        $data['title'] = 'Cambio de Contraseña';
        //$data['upsas'] = $this->usuarios_model->cargar_upsa();

        $this->load->view('templates/header');
        $this->load->view('templates/navegator');
        $this->load->view('usuarios/cambio_contrasenia',$data);
        $this->load->view('templates/footer');
    }

    public function registrar_usuario(){
        $usuario = $this->input->post('n_usuario');

        $clave = password_hash(
            base64_encode(
                hash('sha256', $this->input->post('n_clave'), true)
            ),
            PASSWORD_DEFAULT
        );
        $cedula = $this->input->post('n_cedula');
        $celular = $this->input->post('n_celular');
        $correo = $this->input->post('n_correo');
        if($this->input->post('id_upsa') != 0){
            $id_upsa = $this->input->post('id_upsa');
        }else{
            $id_upsa = NULL;
        }

        if($usuario && $clave && $cedula && $celular && $correo){
            $data = array(
                'usuario' => $usuario,
                'clave' => $clave,
                'cedula' => $cedula,
                'celular' => $celular,
                'correo' => $correo,
                'id_upsa' => $id_upsa
            );

            $this->usuarios_model->registrar_usuario($data);
            $this->session->set_flashdata('registro_exitoso','success');
            redirect('ecopro_upsa');
        }
    }

    public function cambiar_contrasenia(){
        $usuario = $this->input->post('n_usuario');
        $clave = password_hash(
            base64_encode(
                hash('sha256', $this->input->post('n_clave'), true)
            ),
            PASSWORD_DEFAULT
        );

        if($usuario && $clave){
            $data = array(
                'usuario' => $usuario,
                'clave' => $clave
            );

            $this->usuarios_model->cambiar_contrasenia($data);
            //print_r ($data);
            $this->session->set_flashdata('registro_exitoso','success');
            redirect ('usuarios/logout','refresh');
        }
    }

    public function login()
    {

        $usuario = $this->input->post('usuario');
        $clave = $this->input->post('clave');
        $user_info = $this->usuarios_model->login($usuario,$clave);

        //$this->form_validation->set_rules('g-recaptcha-response','Captcha','callback_recaptcha');

        /*if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('pnotify','captcha');
            redirect('ecopro_upsa','refresh');
        }else {*/
            if($user_info){
                $user_data = array(
                    'usuario' => $user_info['usuario'],
                    'id_usuario' => $user_info['id_usuario'],
                    'perfil_id' => $user_info['perfil_id'],
                    'id_upsa' => $user_info['id_upsa'],
                    'nombre_upsa' => $user_info['nombre'],
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($user_data);
                //var_dump($_SESSION);die;
                $this->session->set_flashdata('pnotify','login');
                redirect('principal','refresh');
            }else{
		$this->session->set_flashdata('pnotify','incorrecto');
                redirect('ecopro_upsa','refresh');
            }
        }
    /*}
        /*public function recaptcha($str=''){
          $google_url="https://www.google.com/recaptcha/api/siteverify";
          $secret='6LfVrnMUAAAAAKtXCV2SxVg7lL8M8MCtntO6ZOAu';
          $ip=$_SERVER['REMOTE_ADDR'];
          $url=$google_url."?secret=".$secret."&response=".$str."&remoteip=".$ip;
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($curl, CURLOPT_TIMEOUT, 10);
          curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
          $res = curl_exec($curl);
          curl_close($curl);
          $res= json_decode($res, true);
          //reCaptcha success check
          if($res['success']){
            return TRUE;
          }else{
            $this->form_validation->set_message('recaptcha', 'Por favor rellene el ReCaptcha antes de continuar');
            return FALSE;
          }
      }*/

    public function logout(){
        $this->session->unset_userdata('usuario');
        $this->session->unset_userdata('id_usuario');
        $this->session->unset_userdata('perfil_id');
        $this->session->unset_userdata('id_upsa');
        $this->session->unset_userdata('logged_in');

        $this->session->set_flashdata('pnotify','logout');
        redirect ('ecopro_upsa');
    }

    /* public function validar()
    {
        $usuario = $this->input->post('usuario');
        $clave = $this->input->post('clave');

        $datos = $this->login_model->consulta_login($usuario, $clave);

        if ($datos) {
            $sess_array = array();
            foreach ($datos as $row) {
                $perfil_id = $row->perfil_id;
                $usuario = $row->usuario;

            }
            //echo $usuario;
            $_SESSION['usuario'] = $usuario;
            $_SESSION['perfil_id'] = $perfil_id;

            redirect('principal', 'refresh');
            return true;

        } else {
            redirect('ecopro_upsa', 'refresh');
            return false;
        } */
        //echo json_encode($datos);

}
