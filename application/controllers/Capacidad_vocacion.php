<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Datos Básicos de UPSA con Datos de Coordinador
 *
 * @author Nelly Moreno
 */
class Capacidad_vocacion extends CI_Controller
{

	public $pnoti;

  public function __construct(){
    parent::__construct();
    $this->load->model('capacidad_model', '', true);
    $this->load->model('vocacion_model', '', true);
    $this->load->helper(array('url', 'form'));
    //$this->load->model('auditoria_model', '', TRUE);
    $this->pnoti = 0;
	}

  public function index(){

		if(!$this->session->userdata('logged_in')){
			redirect('');
		}
		if($this->session->userdata('id_upsa')== ''){
			redirect('basicos');
		}
    /* $data = array('titulo' => 'Personas');
    $a=$this->pnoti;
    if ($a ==1){
    $data = array('titulin' => 'Personas', 'pnoti' => 'Se registró exitosamente la información!', 'info' => 'info');
    }else  if ($a ==2){
    $data = array('titulin' => 'Personas', 'pnoti' => 'Ocurrió un Error al tratar de registrar los datos!', 'info' => 'error');
    }else if ($a== 3) {
    $data = array('titulin' => 'Personas', 'pnoti' => 'Se actualizaron correctamente los datos!', 'info' => 'success');
    }else if ($a== 4) {
    $data = array('titulin' => 'Personas', 'pnoti' => 'El ingreso que intenta realizar ya se encuentra registrado', 'info' =>'warnnign');
    }else if ($a== 5) {
    $data = array('titulin' => 'Personas', 'pnoti' => 'Debe llenar todos los para realizar el Registro', 'info' => 'warnnign');
    } */
    $data = array('seccion_n' => 'Registro de Capacidad Operativa', 'sub_titulo' => 'Registro de Capacidad Instalada Operativa');
    $data['sistemas_riego'] = $this->capacidad_model->cargar_sistema_riego();
    $this->load->view('templates/header');
    $this->load->view('templates/navegator', $data);
    $this->load->view('capacidad_vocacion/index', $data);
    $this->load->view('templates/footer');
    //$listado = $this->personas_model->listar_personas();
    // $sistema_riego = $this->capacidad_model->cargar_sistema_riego();
    /*$data = array('inicio' => 'INICIAR SESI&Oacute;N',
    'sistema_riego' => $sistema_riego
    );*/
    //$usuario=$_SESSION['usuario'];
    //$auditoria=new auditoria_model();
    //$rs=$auditoria->registrar_auditoria("Inicio","Ingrso a ventana de personas (usuario:".$usuario.")");
	}

	public function insertar_capacidad_vocacion(){
		$id_upsa = $this->session->userdata('id_upsa');
		$id_sistema_riego = $this->input->post('id_sistema_riego');
		$superficie_estabulada = $this->input->post('superficie_estabulada');
		if($superficie_estabulada == ''){
			$superficie_estabulada = 0;
		}
		$superficie_operativa_agricola = $this->input->post('superficie_operativa_agricola');
		if($superficie_operativa_agricola == ''){
			$superficie_operativa_agricola = 0;
		}
		$superficie_operativa_pecuaria = $this->input->post('superficie_operativa_pecuaria');
		if($superficie_operativa_pecuaria == ''){
			$superficie_operativa_pecuaria = 0;
		}
		$superficie_reserva_natural = $this->input->post('superficie_reserva_natural');
		if($superficie_reserva_natural == ''){
			$superficie_reserva_natural = 0;
		}
		$superficie_aprovechable_agricola = $this->input->post('superficie_aprovechable_agricola');
		if($superficie_aprovechable_agricola == ''){
			$superficie_aprovechable_agricola = 0;
		}
		$superficie_aprovechable_pecuaria = $this->input->post('superficie_aprovechable_pecuaria');
		if($superficie_aprovechable_pecuaria == ''){
			$superficie_aprovechable_pecuaria = 0;
		}
		$total_hectareas1=$superficie_aprovechable_agricola+$superficie_aprovechable_pecuaria;
		$superficie_aprovechable_total = $total_hectareas1;
		if($superficie_aprovechable_total == ''){
			$superficie_aprovechable_total = 0;
		}
		$total_hectareas1 = $superficie_reserva_natural+$total_hectareas1;
		//echo $total_hectareas1;die;
		$total_hectareas = $total_hectareas1;
		if($total_hectareas == ''){
			$total_hectareas = 0;
		}
		$id_upsa = $this->session->userdata('id_upsa');
		$id_clase = $this->input->post('clase[]');
		$count = count($id_clase);
		//var_dump($id_clase);die;

		$this->db->trans_start();

		$this->capacidad_model->insertar_capacidad_operativa(
																																		$id_upsa,
																																		$id_sistema_riego,
																																		$superficie_estabulada,
																																		$superficie_operativa_agricola,
																																		$superficie_operativa_pecuaria,
																																		$superficie_reserva_natural,
																																		$superficie_aprovechable_agricola,
																																		$superficie_aprovechable_pecuaria,
																																		$superficie_aprovechable_total,
																																		$total_hectareas
		);
		for($i=0;$i<$count;$i++){
			$this->vocacion_model->insertar_vocacion($i,$id_upsa,$id_clase);
		}
		$this->db->trans_complete();
		$this->session->set_flashdata('pnotify','insert');
		redirect('capacidad_vocacion');
	}
/*

	public function procesardatos()

	{

	$boton = $this->input->post('boton');

	if($boton=='Actualizar'){

	$datos = $this->personas_model->actualizar_method(
	$this->input->post('cedula'),
	$this->input->post('primer_nombre'),
	$this->input->post('segundo_nombre'),
	$this->input->post('primer_apellido'),
	$this->input->post('segundo_apellido'),
	$this->input->post('nacionalidad'));
	if($datos){
	$this->pnoti=3;
	}
	$usuario=$_SESSION['usuario'];
	$auditoria=new auditoria_model();
	$rs=$auditoria->registrar_auditoria("Inicio","Actualización de una Persona (usuario:".$usuario.")");
	$this->index();

	}

	if($boton=='Guardar'){

	//echo $boton;

	$datos = $this->personas_model->insertar_method(
	$this->input->post('cedula'),
	$this->input->post('primer_nombre'),
	$this->input->post('segundo_nombre'),
	$this->input->post('primer_apellido'),
	$this->input->post('segundo_apellido'),
	$this->input->post('nacionalidad'));
	$usuario=$_SESSION['usuario'];
	$auditoria=new auditoria_model();
	$rs=$auditoria->registrar_auditoria("Inicio","Registro de Nueva Persona (usuario:".$usuario.")");

	if($datos == 1001){
	$this->pnoti=1;

	}else if($datos == 1000)    {
	$this->pnoti=2;
	}else if($datos == 1004){
	$this->pnoti=4;
	}else if($datos == 1005){
	$this->pnoti=5;
	}
	$this->index();
	}

	}*/
}
