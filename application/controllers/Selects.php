<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Selects
 *
 * @author Nelly Moreno
 */
class Selects extends CI_Controller{

    Public $pnoti;
    
   public function __construct()
    {
        parent::__construct();
        $this->load->model('selects_model', '', TRUE);
        $this->load->model('auditoria_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
            $this->pnoti=0;
    }
   
     public function index()
    {
        $data = array('titulo' => 'selects');
          $a=$this->pnoti;
        if ($a ==1){
            $data = array('titulin' => 'selects', 'pnoti' => 'Se registró exitosamente la información!', 'info' => 'info');
        }else  if ($a ==2){
            $data = array('titulin' => 'selects', 'pnoti' => 'Ocurrió un Error al tratar de registrar los datos!', 'info' => 'error');
        }else if ($a== 3) {
                $data = array('titulin' => 'selects', 'pnoti' => 'Se actualizaron correctamente los datos!', 'info' => 'success');
        }else if ($a== 4) {
                $data = array('titulin' => 'selects', 'pnoti' => 'El ingreso que intenta realizar ya se encuentra registrado', 'info' => 'warnnign');
        }else if ($a== 5) {
                $data = array('titulin' => 'selects', 'pnoti' => 'Debe llenar todos los para realizar el Registro', 'info' => 'warnnign');
        }
        $this->load->view('templates/header', $data);
        $data = array('seccion_n' => 'Registro selects', 'sub_titulo_registrar' => 'Registrar nuevos' , 'sub_titulo_actualizar' => 'Modificar existentes', 'titulo_seg_ventana'=> 'Lista Modificable de Trabajadores');
        $this->load->view('templates/navegator', $data);
        $estados = $this->selects_model->cargar_estados();
        //$municipios = $this->selects_model->cargar_municipios();
        //$parroquias = $this->selects_model->cargar_parroquias();
        
        $sub_titulo = 'Datos Basicos';
        $data = array('inicio' => 'INICIAR SESI&Oacute;N',
					'estados' => $estados,
					'municipios' => $municipios,
					'parroquias' => $parroquias,
					'sub_titulo' => $sub_titulo,
					);
		     
        
        
        $this->load->view('selects/index', $data);
        $this->load->view('templates/footer');
        
    }
  
    
     
     public function cargar_estado()
    
    {
        $id_estado = $this->input->post('id_estado');
        $datos = $this->selects_model->consultar_estado_method($id_estado);
        echo json_encode($datos);
        
        }
   
}
