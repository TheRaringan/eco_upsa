<?php
	/**
	 *
	 */
	class rep_cuadros_upsas extends CI_Controller
	{

		Public $pnoti;

	   public function __construct(){
	        parent::__construct();
	        $this->load->model('Rep_cuadros_upsas_model', '', TRUE);
	        $this->load->helper(array('url', 'form'));
	        //$this->load->model('auditoria_model', '', TRUE);
	            $this->pnoti=0;
	    }

		public function index(){
			if(!$this->session->userdata('logged_in')){
	            redirect('');
	        }
			$data['seccion_n'] = 'Reporte UPSAS';
			$a=$this->pnoti;
			$listado_op = $this->Rep_cuadros_upsas_model->listar_op();

			$data = array('inicio' => 'INICIAR SESI&Oacute;N',
	            'titulo' => 'Reportes',
	            'subtitulo' => 'UPSAS',
	            'listado_op' => $listado_op
	        );

			$this->load->view('templates/header');
	        $this->load->view('templates/navegator', $data);
	        $this->load->view('rep_operatividad/index', $data);
	        $this->load->view('templates/footer');
		}

		public function rep_act_prod(){
			if(!$this->session->userdata('logged_in')){
	            redirect('');
	        }
			$data['seccion_n'] = 'Reporte UPSAS';
			$a=$this->pnoti;
			$listado_act_prod = $this->Rep_cuadros_upsas_model->listar_act_prod();

			$data = array('inicio' => 'INICIAR SESI&Oacute;N',
	            'titulo' => 'Reportes',
	            'subtitulo' => 'UPSAS',
	            'listado_act_prod' => $listado_act_prod
	        );

			$this->load->view('templates/header');
	        $this->load->view('templates/navegator', $data);
	        $this->load->view('rep_act_prod/index', $data);
	        $this->load->view('templates/footer');
		}

		public function rep_rec_hib(){
			if(!$this->session->userdata('logged_in')){
	            redirect('');
	        }
			$data['seccion_n'] = 'Reporte UPSAS';
			$a=$this->pnoti;
			$listado_rec_hib = $this->Rep_cuadros_upsas_model->listar_rec_hib();

			$data = array('inicio' => 'INICIAR SESI&Oacute;N',
	            'titulo' => 'Reportes',
	            'subtitulo' => 'UPSAS',
	            'listado_rec_hib' => $listado_rec_hib
	        );

			$this->load->view('templates/header');
	        $this->load->view('templates/navegator', $data);
	        $this->load->view('rep_rec_hib/index', $data);
	        $this->load->view('templates/footer');
		}

		public function rep_voc_tie(){
			if(!$this->session->userdata('logged_in')){
	            redirect('');
	        }
			$data['seccion_n'] = 'Reporte UPSAS';
			$a=$this->pnoti;
			$listado_voc_tie = $this->Rep_cuadros_upsas_model->listar_voc_tie();

			$data = array('inicio' => 'INICIAR SESI&Oacute;N',
	            'titulo' => 'Reportes',
	            'subtitulo' => 'UPSAS',
	            'listado_voc_tie' => $listado_voc_tie
	        );

			$this->load->view('templates/header');
	        $this->load->view('templates/navegator', $data);
	        $this->load->view('rep_voc_tie/index', $data);
	        $this->load->view('templates/footer');
		}
	}
?>
