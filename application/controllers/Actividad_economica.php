<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class Actividad_economica extends CI_Controller{

    public function __construct()
     {
         parent::__construct();
         $this->load->model('select_actividades_model', '', TRUE);
         $this->load->model('actividades_economicas_model', '', TRUE);
         $this->load->helper(array('url', 'form'));
             $this->pnoti=0;
     }


    public function index(){
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			if($this->session->userdata('id_upsa') == ''){
				redirect('basicos');
			}
            $id_upsa =  $this->session->userdata('id_upsa');
			//$data['title'] = "Actividad Economica";
			$data['tipos_actividades'] = $this->select_actividades_model->get_tipos_actividades();
			$data['unidades_medidas'] = $this->select_actividades_model->get_unidades_medidas();
			$data['estados_operacion'] = $this->select_actividades_model->get_estados_operacion();
            $data['listado_act_prod'] = $this->actividades_economicas_model->listar_act_prod($id_upsa);


      $this->load->view('templates/header');
      $this->load->view('templates/navegator');
      $this->load->view('actividad_econ/index',$data);
      $this->load->view('templates/footer');
		}

		public function insertar_actividad(){
			if(!$this->session->userdata('logged_in')){
        redirect('');
			}
			if($this->session->userdata('id_upsa')== ''){
				redirect('basicos');
			}
			$id_upsa = $this->session->userdata('id_upsa');
			$id_tipo_actividad_econ = $this->input->post('id_tipo_actividad_econ');
			$id_actividad_econ = $this->input->post('id_actividad_econ');
			$id_rubro = $this->input->post('id_rubro[]');
			//$id_subrubro = $this->input->post('id_subrubro');
			$produccion_mensual = $this->input->post('produccion_mensual');
			$id_unidad_medida = $this->input->post('id_unidad_medida');
			$id_estado_operacion = $this->input->post('id_estado_operacion');
			if(!$this->input->post('id_laboral')){
				$id_laboral = 1;
			}else{
				$id_laboral = $this->input->post('id_laboral');
			};
			if(!$this->input->post('id_electricidad')){
				$id_electricidad = 1;
			}else{
				$id_electricidad = $this->input->post('id_electricidad');
			};
			if(!$this->input->post('id_mecanico')){
				$id_mecanico = 1;
			}else{
				$id_mecanico = $this->input->post('id_mecanico');
			};
			if(!$this->input->post('id_seguridad')){
				$id_seguridad = 1;
			}else{
				$id_seguridad = $this->input->post('id_seguridad');
			};
			if(!$this->input->post('id_climatico')){
				$id_climatico = 1;
			}else{
				$id_climatico = $this->input->post('id_climatico');
			};
			if(!$this->input->post('id_materia_prima')){
				$id_materia_prima = 1;
			}else{
				$id_materia_prima = $this->input->post('id_materia_prima');
			};
			$observaciones = $this->input->post('observaciones');

			$count = count($id_rubro);

			for($i=0;$i<$count;$i++){
				$this->actividades_economicas_model->insertar_actividades(	$i,
																																		$id_upsa,
																																		$id_tipo_actividad_econ,
																																		$id_actividad_econ,
																																		$id_rubro,
																																		//$id_subrubro,
																																		$produccion_mensual,
																																		$id_unidad_medida,
																																		$id_estado_operacion,
																																		$id_laboral,
																																		$id_electricidad,
																																		$id_mecanico,
																																		$id_seguridad,
																																		$id_climatico,
																																		$id_materia_prima,
																																		$observaciones
				);
			}
			$this->session->set_flashdata('pnotify','insert');
			redirect('Actividad_economica');
		}
  }
