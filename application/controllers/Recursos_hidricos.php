<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class Recursos_hidricos extends CI_Controller{

    public function __construct()
     {
         parent::__construct();
         $this->load->model('select_recursos_model', '', TRUE);
         $this->load->model('recursos_hidricos_model','',TRUE);
         //$this->load->helper(array('url', 'form'));
             $this->pnoti=0;
     }


    public function index(){
      if(!$this->session->userdata('logged_in')){
        redirect('');
      }
      if($this->session->userdata('id_upsa') == ''){
				redirect('basicos');
			}
			$data['title'] = "Recursos Hidricos";
      $data['recursos'] = $this->select_recursos_model->get_tipos_recursos();

     /*  $this->form_validation->set_rules();
      $this->form_validation->set_rules(); */

      $this->load->view('templates/header');
      $this->load->view('templates/navegator');
      $this->load->view('recursos_hidricos/index',$data);
      $this->load->view('templates/footer');
		}

		public function insertar_recurso(){
			$id_upsa = $this->session->userdata('id_upsa');
			$id_tipo_recurso_hidr = $this->input->post('tipo');
			$id_recurso_hidr = $this->input->post('recurso[]');
			$count = count($id_recurso_hidr);
			for($i=0;$i<$count;$i++){
				$this->recursos_hidricos_model->insertar_recursos($i,$id_upsa,$id_tipo_recurso_hidr,$id_recurso_hidr);
      }
      $this->session->set_flashdata('pnotify','insert');
			redirect('recursos_hidricos');
		}
  }
