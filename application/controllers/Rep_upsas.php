<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Datos Básicos de UPSA con Datos de Coordinador
 *
 * @author Nelly Moreno
 */
class Rep_upsas extends CI_Controller{

    Public $pnoti;

   public function __construct()
    {
        parent::__construct();
        $this->load->model('Rep_upsas_model', '', TRUE);
        $this->load->helper(array('url', 'form'));
        //$this->load->model('auditoria_model', '', TRUE);
            $this->pnoti=0;
    }

     public function index(){
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $data['seccion_n'] = 'Reporte UPSAS';
        $a=$this->pnoti;

        $listado = $this->Rep_upsas_model->listar_upsas();

        $data = array('inicio' => 'INICIAR SESI&Oacute;N',
            'titulo' => 'Datos Basicos de la UPSA',
            'subtitulo' => 'Coordinador',
             'Listado' => $listado
        );
        $this->load->view('templates/header');
        $this->load->view('templates/navegator', $data);
        $this->load->view('rep_upsas/index', $data);
        $this->load->view('templates/footer');
        //$usuario=$_SESSION['usuario'];
       //$auditoria=new auditoria_model();
        //$rs=$auditoria->registrar_auditoria("Inicio","Ingrso a ventana de personas (usuario:".$usuario.")");
    }
  }
