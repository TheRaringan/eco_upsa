<script language="JavaScript" type="text/javascript">

var pagina="x_auditoria"
function redireccionar() 
{
location.href=pagina
} 


</script>
             



<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                       <?=$seccion_n?>
                     
                  </h3>
              </div>

         
            </div>
            <div class="clearfix"></div>

            <div class="row">

          

   
              
                 
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <div class="col-md-10 col-sm-12 col-xs-12">

                    <h2><?=$sub_titulo?><small>(Listado)</small></h2>
                          </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">

                    <button type="button" class="btn btn-dark btn-xs" onclick="redireccionar()"><span class="fa fa-download" > </span> Generar Reporte en XLS</button>
                         </div>
                    <ul class="nav navbar-right panel_toolbox">
            
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">
                          <p class="text-muted font-13 m-b-30">
                       Auditoría registradas hasta la fecha
                          </p>
                          
                          
                           

                          <table id="datatable-keytable" class="table table-striped table-bordered">
                            <thead>
                              <tr>

                                <th>Código</th>
                                <th>Sección</th>
                                <th>Acción</th>
                                <th>Campo Clave</th>
                                <th>Usuario</th>
                                <th>IP</th>
                                <th>Fecha</th>   
                                <th>Hora</th>  
                                 
                                   
                              </tr>
                            </thead>


                            <tbody>
 
<?php foreach ($Listado as $row) : ?>
                    <tr>
                        <td><?=$row[0]?></td>
                        <td><?=$row[1]?></td>
                        <td><?=$row[2]?></td>
                        <td><?=$row[3]?></td>
                        <td><?=$row[4]?></td>
                        <td><?=$row[5]?></td>
                        <td><?=$row[6]?></td>
                        <td><?=$row[7]?></td>
            
                  
                    </tr>
<?php endforeach; ?>

                           </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Desarrollado por Nelly Moreno para PDVSA La Estancia <a href="#"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

   
