<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ECOPRO - UPSA</title>

    <!-- Bootstrap -->
    <link href="<?=base_url()?>plantilla/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
     <link rel="stylesheet" href="<?=base_url()?>mapas/demo.css">
     <link rel="stylesheet" href="<?=base_url()?>mapas/demo-print.css">

    <link href="<?=base_url()?>plantilla/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?=base_url()?>plantilla/vendors/iCheck/skins/flat/green.css" rel="stylesheet">


    <!-- bootstrap-wysiwyg -->
    <link href="<?=base_url()?>plantilla/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?=base_url()?>plantilla/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?=base_url()?>plantilla/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="<?=base_url()?>plantilla/vendors/starrr/dist/starrr.css" rel="stylesheet">

  <!-- Datatables -->
    <link href="<?=base_url()?>plantilla/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- NProgress -->
    <link href="<?=base_url()?>plantilla/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=base_url()?>plantilla/css/custom.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/css/custom.min.css" rel="stylesheet"


   <link href="<?=base_url()?>plantilla/vendors/switchery/dist/switchery.min.css" rel="stylesheet">

   <!-- Validaciones -->
	<script type="text/javascript" src="<?=base_url()?>/validaciones/valida.js"></script>
	<!-- Pnoti -->

    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <script src="<?=base_url()?>plantilla/vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/echarts/map/js/world.js"></script>
  </head>
  <body class="nav-md">
  <!-- flashdata -->
  <?php if($this->session->flashdata('pnotify')): ?>
  <?='<input type="hidden" id="myPnotify" value="'.$this->session->flashdata('pnotify').'">'?>
  <?php else: ?>
	<?php endif; ?>
  <!-- /flash data -->
