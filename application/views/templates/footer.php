<footer>
  <div class="pull-right">
    DELAGRO Desarrollado por: Oficina de Tecnología de la Información y Comunicaciones

  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- Variables -->
<script>
	var base_url = '<?=base_url()?>';
	var id_upsa = '<?=$this->session->userdata('id_upsa')?>';
</script>
<!-- jQuery -->
<script src="<?=base_url()?>plantilla/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url()?>plantilla/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>plantilla/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?=base_url()?>plantilla/vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="<?=base_url()?>plantilla/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url()?>plantilla/vendors/iCheck/icheck.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?=base_url()?>plantilla/js/moment/moment.min.js"></script>
<script src="<?=base_url()?>plantilla/js/datepicker/daterangepicker.js"></script>
<!-- bootstrap-wysiwyg -->
<script src="<?=base_url()?>plantilla/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="<?=base_url()?>plantilla/vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="<?=base_url()?>plantilla/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- jquery.inputmask -->
<script src="<?=base_url()?>plantilla/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<!-- Switchery -->
<script src="<?=base_url()?>plantilla/vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>plantilla/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="<?=base_url()?>plantilla/vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="<?=base_url()?>plantilla/vendors/autosize/dist/autosize.min.js"></script>
<!-- jQuery autocomplete -->
<script src="<?=base_url()?>plantilla/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<!-- starrr -->
<script src="<?=base_url()?>plantilla/vendors/starrr/dist/starrr.js"></script>
<!-- starrr -->
<script src="<?=base_url()?>plantilla/vendors/raphael/raphael.min.js"></script>
<!-- <script src="<?=base_url()?>plantilla/vendors/morris.js/morris.min.js"></script> -->

<!-- Datatables -->
<script src="<?=base_url()?>plantilla/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?=base_url()?>plantilla/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?=base_url()?>plantilla/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="<?=base_url()?>plantilla/vendors/numeral/numeral.min.js"></script>

<!-- PNotify -->

<script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.js"></script>
<script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.js"></script>
<script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.js"></script>
<script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<!-- data tables -->

<script src="<?=base_url()?>js/carga_actividades.js"></script>
<script src="<?=base_url()?>js/carga_recursos.js"></script>
<script src="<?=base_url()?>js/dependientes.js"></script>
<script src="<?=base_url()?>js/dependiente_ubicacion.js"></script>
<script src="<?=base_url()?>js/parse-float.js"></script>
<script src="<?=base_url()?>js/alertas.js"></script>
<script src="<?=base_url()?>assets/js/morris.js"></script>
<script src="<?=base_url()?>js/verificar_basico.js"></script>


<script src="<?=base_url()?>validaciones/validaciones_basico.js"></script>
<script src="<?=base_url()?>validaciones/validaciones_ubicacion.js"></script>
<script src="<?=base_url()?>validaciones/validaciones_capacidad.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?=base_url()?>plantilla/js/custom.js"></script>
<!-- Datatables -->

<script src="<?=base_url()?>mapas/mapa.js"></script>
<script src="<?=base_url()?>mapas/java.js"></script>

<script>
  $(document).ready(function () {
    var handleDataTableButtons = function () {
      if ($("#datatable-buttons").length) {
        $("#datatable-buttons").DataTable({
          dom: "Bfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function () {
      "use strict";
      return {
        init: function () {
          handleDataTableButtons();
        }
      };
    }();

    $('#datatable').dataTable();
    $('#datatable-keytable').DataTable({
      keys: true
    });

    $('#datatable-responsive').DataTable();

    $('#datatable-scroller').DataTable({
      ajax: "js/datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });

    var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });

    TableManageButtons.init();
  });
</script>
<!-- /Datatables -->

<!-- jquery.inputmask -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();
  });
</script>


<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function () {
    $('#birthday').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_4"
    }, function (start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });
  });
</script>
<!-- /bootstrap-daterangepicker -->

<!-- bootstrap-wysiwyg -->
<script>
  $(document).ready(function () {
    $('.xcxc').click(function () {
      $('#descr').val($('#editor').html());
    });
  });

  $(document).ready(function () {
    function initToolbarBootstrapBindings() {
      var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
        'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
        'Times New Roman', 'Verdana'
      ],
        fontTarget = $('[title=Font]').siblings('.dropdown-menu');
      $.each(fonts, function (idx, fontName) {
        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
      });
      $('a[title]').tooltip({
        container: 'body'
      });
      $('.dropdown-menu input').click(function () {
        return false;
      })
        .change(function () {
          $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
        })
        .keydown('esc', function () {
          this.value = '';
          $(this).change();
        });

      $('[data-role=magic-overlay]').each(function () {
        var overlay = $(this),
          target = $(overlay.data('target'));
        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
      });

      if ("onwebkitspeechchange" in document.createElement("input")) {
        var editorOffset = $('#editor').offset();

        $('.voiceBtn').css('position', 'absolute').offset({
          top: editorOffset.top,
          left: editorOffset.left + $('#editor').innerWidth() - 35
        });
      } else {
        $('.voiceBtn').hide();
      }
    }

    function showErrorAlert(reason, detail) {
      var msg = '';
      if (reason === 'unsupported-file-type') {
        msg = "Unsupported format " + detail;
      } else {
        console.log("error uploading file", reason, detail);
      }
      $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }

    initToolbarBootstrapBindings();

    $('#editor').wysiwyg({
      fileUploadError: showErrorAlert
    });

    window.prettyPrint && prettyPrint();
  });
</script>
<!-- /bootstrap-wysiwyg -->

<!-- Select2 -->
<script>
  $(document).ready(function () {
    $(".select2_single").select2({
      placeholder: "Seleccione",
      allowClear: true
    });
    $(".select2_group").select2({});
    $(".select2_multiple").select2({
      maximumSelectionLength: 4,
      placeholder: "With Max Selection limit 4",
      allowClear: true
    });
  });
</script>
<!-- /Select2 -->

<!-- jQuery Tags Input -->
<script>
  function onAddTag(tag) {
    alert("Added a tag: " + tag);
  }

  function onRemoveTag(tag) {
    alert("Removed a tag: " + tag);
  }

  function onChangeTag(input, tag) {
    alert("Changed a tag: " + tag);
  }

  $(document).ready(function () {
    $('#tags_1').tagsInput({
      width: 'auto'
    });
  });
</script>
<!-- /jQuery Tags Input -->

<!-- Parsley -->
<!--  <script>
      $(document).ready(function() {
        $.listen('parsley:field:validate', function() {
          validateFront();
        });
        $('#demo-form .btn').on('click', function() {
          $('#demo-form').parsley().validate();
          validateFront();
        });
        var validateFront = function() {
          if (true === $('#demo-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
          } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
          }
        };
      });

      $(document).ready(function() {
        $.listen('parsley:field:validate', function() {
          validateFront();
        });
        $('#demo-form2 .btn').on('click', function() {
          $('#demo-form2').parsley().validate();
          validateFront();
        });
        var validateFront = function() {
          if (true === $('#demo-form2').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
          } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
          }
        };
      });
      try {
        hljs.initHighlightingOnLoad();
      } catch (err) {}
    </script> -->
<!-- /Parsley -->

<!-- Autosize -->
<script>
  $(document).ready(function () {
    autosize($('.resizable_textarea'));
  });
</script>
<!-- /Autosize -->


<!-- /jQuery autocomplete -->

<!-- Starrr -->
<script>
  $(document).ready(function () {
    $(".stars").starrr();

    $('.stars-existing').starrr({
      rating: 4
    });

    $('.starrr').on('click', function (e) {
      e.preventDefault();
    });

    $('.stars').on('starrr:change', function (e, value) {
      $('.stars-count').html(value);
    });

    $('.stars-existing').on('starrr:change', function (e, value) {
      $('.stars-count-existing').html(value);
    });
  });
</script>
<!-- /Starrr -->




<!-- jQuery Smart Wizard -->
<script src="<?=base_url()?>/plantilla/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>


<!-- jQuery Smart Wizard -->
<script>
  $(document).ready(function () {
    $('#wizard').smartWizard();

    $('#wizard_verticle').smartWizard({
      transitionEffect: 'slide'
    });

    $('.buttonNext').addClass('btn btn-success');
    $('.buttonPrevious').addClass('btn btn-primary');
    $('.buttonFinish').addClass('btn btn-default');
  });
</script>


<script>
  $(document).ready(function () {
    var cnt = 10;

    TabbedNotification = function (options) {
      var message = "<div id='ntf" + cnt + "' class='text alert-" + options.type + "' style='display:none'><h2><i class='fa fa-bell'></i> " + options.title +
        "</h2><div class='close'><a href='javascript:;' class='notification_close'><i class='fa fa-close'></i></a></div><p>" + options.text + "</p></div>";

      if (!document.getElementById('custom_notifications')) {
        alert('doesnt exists');
      } else {
        $('#custom_notifications ul.notifications').append("<li><a id='ntlink" + cnt + "' class='alert-" + options.type + "' href='#ntf" + cnt + "'><i class='fa fa-bell animated shake'></i></a></li>");
        $('#custom_notifications #notif-group').append(message);
        cnt++;
        CustomTabs(options);
      }
    };

    CustomTabs = function (options) {
      $('.tabbed_notifications > div').hide();
      $('.tabbed_notifications > div:first-of-type').show();
      $('#custom_notifications').removeClass('dsp_none');
      $('.notifications a').click(function (e) {
        e.preventDefault();
        var $this = $(this),
          tabbed_notifications = '#' + $this.parents('.notifications').data('tabbed_notifications'),
          others = $this.closest('li').siblings().children('a'),
          target = $this.attr('href');
        others.removeClass('active');
        $this.addClass('active');
        $(tabbed_notifications).children('div').hide();
        $(target).show();
      });
    };

    CustomTabs();

    var tabid = idname = '';

    $(document).on('click', '.notification_close', function (e) {
      idname = $(this).parent().parent().attr("id");
      tabid = idname.substr(-2);
      $('#ntf' + tabid).remove();
      $('#ntlink' + tabid).parent().remove();
      $('.notifications a').first().addClass('active');
      $('#notif-group div').first().css('display', 'block');
    });
  });
</script>

<?php
// if($titulin){
//   $mensaje='eeee';
//   $estilo='dark';
//    echo "
//        <script>
//         $(document).ready(function() {
//           new PNotify({
//             title: '".$titulin."',
//             type: '".$info."',
//             text: '".$pnoti."',
//             nonblock: {
//                 nonblock: false
//             },";
//                     echo "
//             styling: 'bootstrap3',
//             hide: false,
//             before_close: function(PNotify) {
//               PNotify.update({
//                 title: PNotify.options.title + ' - Enjoy your Stay',
//                 before_close: null
//               });

//               PNotify.queueRemove();

//               return false;
//             }
//           });

//         });
//       </script>";
//   }
  ?>
</body>

</html>
