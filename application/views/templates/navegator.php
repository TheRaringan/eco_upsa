    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?=base_url()?>principal" class="site_title"><i class="fa fa-share-alt-square"></i> <span>ECOPRO - UPSA </span></a>
            </div>

            <div class="clearfix"></div>
            <!-- /menu prile quick info -->
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Inicio</h3>
                <ul class="nav side-menu">
                  <li><a href="<?=base_url()?>principal"><i class="fa fa-home"></i> Página Principal </a></li>
                </ul>
              </div>
              <?php if($this->session->userdata('perfil_id') == 333):?>
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i> Registro <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?=base_url()?>basicos">Datos Básicos</a></li>
                            <li><a href="<?=base_url()?>ubicacion">Ubicación</a></li>
                            <li><a href="<?=base_url()?>capacidad_vocacion">Capacidad</a></li>
                            <li><a href="<?=base_url()?>Actividad_economica">Actividades Económicas</a></li>
                            <li><a href="<?=base_url()?>Recursos_hidricos">Recursos Hidricos</a></li>
                            <!--<li><a href="<?=base_url()?>Maquinaria">Registro Maquinaria</a></li>
                            <li><a href="<?=base_url()?>Infraestructura">Infraestructuta de Apoyo a la Producción</a></li>-->
                        </ul>
                    </li>
                     <li><a href="<?=base_url()?>usuarios/cambio_contrasenia"><i class="fa fa-plus-square"></i>Cambiar Contraseña</a></li>
                </ul>
              </div>
              <?php elseif($this->session->userdata('perfil_id') == 777):?>
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3>Administrador</h3>
                    <ul class="nav side-menu">
                      <!-- <li><a href="<?=base_url()?>dashboard/report"><i class="fa fa-gear"></i> Panel de Control </a></li> -->
                      <li><a><i class="fa fa-bar-chart-o"></i>Reportes / Mapas<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?=base_url()?>Dashboard">Mapa</a></li>
                          <li><a href="<?=base_url()?>mapa">Mapa 2</a></li>
                          <li><a href="<?=base_url()?>rep_upsas">UPSAS Registradas</a></li>
                          <li><a href="<?=base_url()?>rep_informacion_upsas">Reporte</a></li>
                          <li><a href="<?=base_url()?>rep_cuadros_upsas">Reporte de Operatividad</a></li>
                          <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_act_prod">Reporte de Act. Productiva</a></li>
                          <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_rec_hib">Reporte de Recursos Hidricos</a></li>
                          <li><a href="<?=base_url()?>rep_cuadros_upsas/rep_voc_tie">Reporte de Voc. Tierra</a></li>
                        </ul>
                      </li>
                      <li><a href="<?=base_url()?>usuarios/registro"><i class="fa fa-plus-square"></i>Registrar Nuevos Usuarios</a></li>
		      <li><a href="<?=base_url()?>usuarios/cambio_contrasenia"><i class="fa fa-plus-square"></i>Cambiar Contraseña</a></li>
                    </ul>
                  </div>
                </div>
            <?php endif;?>
            </div>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <!-- <a data-toggle="tooltip" data-placement="top" title="Configuracion" class="col-sm-1">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a> -->
              <a data-toggle="tooltip" data-placement="top" title="Cerrar Sesión" href="<?=base_url()?>usuarios/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <div class="">
                   <a href="#"><img align="right" src="<?php echo base_url(); ?>assets/images/corpo.png" style="height: 70px" alt=""></a>
              </div>

            </nav>
        </div>
        </div>
        <!-- /top navigation -->
