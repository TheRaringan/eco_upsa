<div class="right_col" role="main">
	<div class="">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Upsas Registradas y Coordinador</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="datatable-keytable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Estado</th>
								<th>Nombre Upsa</th>
								<th>Cedula</th>
								<th>Coordinador Nombre</th>
								<th>Telf. Coordinador</th>
								<th>Correo</th>
							</tr>
						</thead>
						<tbody>
							<?php if($Listado != FALSE):?>
							<?php foreach ($Listado as $row) : ?>
					       		<tr>
									<td><?=$row[5]?></td>
						            <td><?=$row[0]?></td>
						            <td><?=$row[1]?></td>
						            <td><?=$row[2]?></td>
						            <td><?=$row[3]?></td>
						            <td><?=$row[4]?></td>
				        		</tr>
				    		<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	  $('#datatable-buttons').DataTable({
			"language": {
				"decimal": "",
				"emptyTable": "No hay información",
				"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
				"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
				"infoFiltered": "(Filtrado de _MAX_ total entradas)",
				"infoPostFix": "",
				"thousands": ",",
				"lengthMenu": "Mostrar _MENU_ Entradas",
				"loadingRecords": "Cargando...",
				"processing": "Procesando...",
				"search": "Buscar:",
				"zeroRecords": "Sin resultados encontrados",
				"paginate": {
					"first": "Primero",
					"last": "Ultimo",
					"next": "Siguiente",
					"previous": "Anterior"
				}
			},
	  });
	});
</script>
