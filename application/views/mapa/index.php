<!-- ruta: http://localhost/mapa_upsa/index.php/mapa/index -->
<!--cambiar puerto 5433 a 5432 -->

<!-- page content -->
<div class="right_col" role="main">
  <form action="recursos_hidricos/insertar_recurso" method="POST" id="demo-form2">
    <div class="x_panel">
      <div class="x_title">
        <div>
          <h3>
            <?=$title?>
          </h3>
        </div>
      </div>
      <div class="x_content">
        <div id="zulia">
            <h2>Zulia </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_zulia'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_zulia'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_zulia'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_zulia'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_zulia'])?>%</h4>
        </div>

        <div id="sucre">
            <h2>Sucre </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_sucre'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_sucre'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_sucre'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_sucre'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_sucre'])?>%</h4>
        </div>

        <div id="falcon">
            <h2> Falcón</h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_falcon'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_falcon'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_falcon'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_falcon'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_falcon'])?>%</h4>
        </div>

        <div id="cojedes">
            <h2> Cojedes </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_cojedes'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_cojedes'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_cojedes'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_cojedes'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_cojedes'])?>%</h4>           
        </div>

        <div id="carabobo">
            <h2> Carabobo </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_carabobo'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_carabobo'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_carabobo'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_carabobo'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_carabobo'])?>%</h4>         
        </div>
        
        <div id="aragua">
            <h2 id="">Aragua </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_aragua'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_aragua'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_aragua'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_aragua'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_aragua'])?>%</h4>
        </div>

        <!--<div id="lasaves">
            <h4>Total Upsa Agricolas:0</h4>
            <h4>Total Upsa Pecuarias:0</h4>
            <h4>% Superficie Operativa Agricola:0%</h4>
            <h4>% Superficie Operativa Pecuaria:0%</h4>
        </div>

        <div id="roques">
            <h2> Archipiélago Los Roques </h2>
            <h4>Total Upsa Agricolas:0</h4>
            <h4>Total Upsa Pecuarias:0</h4>
            <h4>% Superficie Operativa Agricola:0%</h4>
            <h4>% Superficie Operativa Pecuaria:0%</h4>
        </div>

        <div id="orchila">
            <h2> Isla La Orchila </h2>
            <h4>Total Upsa Agricolas:0</h4>
            <h4>Total Upsa Pecuarias:0</h4>
            <h4>% Superficie Operativa Agricola:0%</h4>
            <h4>% Superficie Operativa Pecuaria:0%</h4>
        </div>

        <div id="tortuga">
            <h2> Isla La Tortuga </h2>
            <h4>Total Upsa Agricolas:0</h4>
            <h4>Total Upsa Pecuarias:0</h4>
            <h4>% Superficie Operativa Agricola:0%</h4>
            <h4>% Superficie Operativa Pecuaria:0%</h4>
        </div>

        <div id="blanquilla">
            <h2> Isla La Blanquilla </h2>
            <h4>Total Upsa Agricolas:0</h4>
            <h4>Total Upsa Pecuarias:0</h4>
            <h4>% Superficie Operativa Agricola:0%</h4>
            <h4>% Superficie Operativa Pecuaria:0%</h4>
        </div>
        
        <div id="testigos">
            <h2>Archipiélago Los Testigos </h2>
            <h4>Total Upsa Agricolas:0</h4>
            <h4>Total Upsa Pecuarias:0</h4>
            <h4>% Superficie Operativa Agricola:0%</h4>
            <h4>% Superficie Operativa Pecuaria:0%</h4>
        </div>

        <div id="hermanos">
            <h2> Islas Los Hermanos</h2>
            <h4>Total Upsa Agricolas:0</h4>
            <h4>Total Upsa Pecuarias:0</h4>
            <h4>% Superficie Operativa Agricola:0%</h4>
            <h4>% Superficie Operativa Pecuaria:0%</h4>
        </div>-->

        <div id="nuevaesparta">
            <h2>Nueva Esparta </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_nuevaesparta'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_nuevaesparta'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_nuevaesparta'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_nuevaesparta'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_nuevaesparta'])?>%</h4>
        </div>

        <div id="guarico">
            <h2>Guárico </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_guarico'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_guarico'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_guarico'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_guarico'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_guarico'])?>%</h4>
        </div>

        <div id="apure">
            <h2> Apure </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_apure'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_apure'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_apure'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_apure'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_apure'])?>%</h4>
        </div>

        <div id="sucre">
            <h2> Sucre </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_sucre'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_sucre'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_sucre'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_sucre'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_sucre'])?>%</h4>
        </div>

        <div id="anzoategui">
            <h2> Anzoátegui</h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_anzoategui'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_anzoategui'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_anzoategui'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_anzoategui'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_anzoategui'])?>%</h4>
        </div>

        <div id="monagas">
            <h2> Monagas </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_monagas'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_monagas'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_monagas'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_monagas'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_monagas'])?>%</h4>
        </div>

        <div id="bolivar">
            <h2>  Bolívar</h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_bolivar'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_bolivar'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_bolivar'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_bolivar'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_bolivar'])?>%</h4>
        </div>

        <div id="amazonas">
            <h2> Amazonas </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_amazonas'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_amazonas'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_bolivar'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_amazonas'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_amazonas'])?>%</h4>
        </div>

        <div id="amacuro">
            <h2> Delta Amacuro </h2>
            <h4>Total Upsa Agricolas:<?=$totales_agric['total_upsa_agricolas_delta'] ?></h4>
            <h4>Total Upsa Pecuarias:<?=$totales_pecua['total_upsa_pecuarias_delta'] ?></h4>
            <h4>Total Upsas :<?=$total_upsa['cant_upsas_delta'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_delta'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_delta'])?>%</h4>
        </div>

        <div id="lara">
            <h2> Lara </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_lara'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_lara'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_lara'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_lara'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_lara'])?>%</h4>
        </div>

        <div id="portuguesa">
            <h2> Portuguesa </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_portugresa'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_portugresa'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_portugresa'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_portugesa'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_portugesa'])?>%</h4>
        </div>

        <div id="yaracuy">
            <h2> Yaracuy </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_yaracuy'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_yaracuy'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_yaracuy'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_yaracuy'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_yaracuy'])?>%</h4>
        </div>

        <div id="miranda">
            <h2> Miranda </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_miranda'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_miranda'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_miranda'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_miranda'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_miranda'])?>%</h4>
        </div>

        <div id="falcon">
            <h2> Falcón </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_falcon'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_falcon'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_falcon'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_falcon'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_falcon'])?>%</h4>
        </div>

        <div id="vargas">
            <h2> Vargas  </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_vargas'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_vargas'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_vargas'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_vargas'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_vargas'])?>%</h4>
        </div>

        <div id="capital">
            <h2> Distrito Capital </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_distcapital'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_distcapital'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_distcapital'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_distcapital'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_distcapital'])?>%</h4>
        </div>

        <div id="trujillo">
            <h2> Trujillo </h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_trujillo'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_trujillo'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_trujillo'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_trujillo'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_trujillo'])?>%</h4>
        </div>

        <div id="tachira">
            <h2>Táchira</h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_tachira'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_tachira'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_tachira'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_tachira'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_tachira'])?>%</h4>
        </div>

        <div id="barinas">
            <h2> Barinas</h2>
            <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_barinas'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_barinas'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_barinas'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_barinas'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_barinas'])?>%</h4>
        </div>

        <div id="merida">
        <h2> Mérida </h2>
        <h4>Total Upsa Agricolas: <?=$totales_agric['total_upsa_agricolas_merida'] ?></h4>
            <h4>Total Upsa Pecuarias: <?=$totales_pecua['total_upsa_pecuarias_merida'] ?></h4>
            <h4>Total Upsas : <?=$total_upsa['cant_upsas_merida'] ?></h4>
            <h4>% Superficie Operativa Agricola: <?=floatval($totale_p_agric['p_agric_merida'])?>%</h4>
            <h4>% Superficie Operativa Pecuaria: <?=floatval($total_p_pecua['p_pecua_merida'])?>%</h4>
        </div> 

        <div class="row">
        <div class="col-md-offset-3 col-md-9">
          <div id="canvas"> 
            <div id="paper"></div>        
          </div> 
          </div> 
        </div>
        
      </div>
    </div>
  </form>
</div>
<!-- /page content -->



