<!-- page content -->
<div class="right_col" role="main">
  <form action="cambiar_contrasenia" method="POST" id="demo-form2">
    <div class="x_panel">
      <div class="x_title">
        <div>
          <h3>
            <?=$title?>
          </h3>
        </div>
      </div>
      <div class="x_content">
        <div class="form-group">
          <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs12">
              <label>Usuario</label>
              <input type="text" name="n_usuario" class="form-control" value="<?=$this->session->userdata('usuario')?>" readonly>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-offset-3 col-md-3 col-sm-12 col-xs12">
              <label>Contraseña</label>
              <input type="password" name="n_clave" class="form-control" placeholder="Ingrese Contraseña">
            </div>
            <div class="col-md-3 col-sm-12 col-xs12">
              <label>Contraseña</label>
              <input type="password" name="repetir_clave" class="form-control" placeholder="Repita Contraseña">
            </div>
          </div>
          <br>
        </div>
        <br>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-offset-5 col-md-6">
            <button type="button" class="btn btn-primary">Cancelar</button>
            <button type="submit" id="" class="btn btn-success">Registrar</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<!-- /page content -->
