<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ECOPRO | UPSA</title>

    <!-- Bootstrap -->
    <link href="<?=base_url()?>plantilla/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url()?>plantilla/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= base_url() ?>plantilla/css/custom.css" rel="stylesheet">

    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

  <body style="background:#F7F7F7;">
    <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>

      <div id="wrapper">
        <div id="login" class=" form">
          <section class="login_content">
              <div class="login-logo">
                  <a href="#"><img src="<?php echo base_url(); ?>assets/images/corpo.png" style="height: 150px; margin-top:-20px" alt=""></a>
                </div>
            <form name="frmLogin" id="frmLogin" method="post" action ="usuarios/login">

              <h1>ECOPRO - UPSA</h1>
              <div>
                <input type="text" class="form-control" name="usuario" id="usuario"  placeholder="Usuario" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="clave" id="clave" placeholder="Clave" required="" />
              </div>
              <!--CATCHA-->
              <!--<div id="captcha-div">
                <div class="g-recaptcha" data-sitekey="6LfVrnMUAAAAAP3bgz9h68cSZ5sgT3qVbUMQNs3I"></div>
            </div>-->
              <br>
              <!-- //////////////////// -->
              <div class="col-xs-offset-3 col-md-3 col-sm-3 col-xs-3">
				    <input type="submit" value="Entrar" class="btn btn-default submit"/>
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-share" style="font-size: 26px;"></i> Acceso</h1>
                  <p>©2016 Desarrollado por Oficina de Tenología de la Información DELAGRO</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>

    <!-- jQuery -->
    <script src="<?=base_url()?>plantilla/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?=base_url()?>plantilla/vendors/bootstrap/dist/js/bootstrap.min.js"></script>


<script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- PNotify -->

    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?=base_url()?>plantilla/vendors/pnotify/dist/pnotify.nonblock.js"></script>

    <script src="<?=base_url()?>js/alertas.js"></script>
  </body>
</html>
