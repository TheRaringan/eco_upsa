<!-- formulario para guardar Datos básico de la UPSA -->
<div class="right_col" role="main">
<?= form_open('basicos/procesardatos', array('name' => 'formulario', 'id' => 'formulario_basico'))?>
	<div class="">
		<!--//boton buscar-->
		<div class="clearfix"></div>
		<div class="row">
			<div class="x_panel">
				<div class="x_title">
					<h2>
						<?=$titulo?>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Nombre de la UPSA</label>
						<input type="text" name="nombre_upsa" id="nombre_upsa" placeholder="Nombre de la UPSA" class="form-control" onKeyPress="return valida(event,this,0,100)"
						 onBlur="valida2(this,0,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Código SUNAGRO</label>
						<input type="text" name="cod_sunagro" id="cod_sunagro" placeholder="Código Sunagro" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();"
						 onKeyPress="return valida(event,this,10,20)" onBlur="valida2(this,10,20)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Código INSAI</label>
						<input type="text" name="cod_insai" id="cod_insai" placeholder="Código INSAI" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();"
						 onKeyPress="return valida(event,this,10,20)" onBlur="valida2(this,10,20)">
					</div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>AgroPatria mas cercano</label>
						<select name="id_agropatria" id="id_agropatria" class="select2_single form-control" tabindex="-1">
							<option value='0'> Agropatria </option>
							<?php foreach ($agropatria as $row) : ?>
							<option value='<?=$row[0]?>'>
								<?=$row[1]?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Tenencia de la Tierra</label>
						<select name="id_tenencia_tierra" id="id_tenencia_tierra" class="select2_single form-control" tabindex="-1">
							<option value='0'> Tenencia de la Tierra </option>
							<?php foreach ($tenencia_tierra as $row) : ?>
							<option value='<?=$row[0]?>'>
								<?=$row[1]?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Relación con la Corporación</label>
						<select name="id_relacion_corpo" id="id_relacion_corpo" class="select2_single form-control" tabindex="-1">
							<option value='0'> Relación con La Corporación </option>
							<?php foreach ($relacion_corpo as $row) : ?>
							<option value='<?=$row[0]?>'>
								<?=$row[1]?>
							</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="x_panel">
				<div class="x_title">
					<h2>
						<?=$subtitulo?>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Cédula</label>
						<input type="text" name="cedula" id="cedula" placeholder="Cédula" class="form-control" onKeyPress="return valida(event,this,10,100)"
						 onBlur="valida2(this,10,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Nombre</label>
						<input type="text" name="nombre" id="nombre_coordinador" placeholder="Nombre" class="form-control" onKeyPress="return valida(event,this,0,100)"
						 onBlur="valida2(this,0,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Apellido</label>
						<input type="text" name="apellido" id="apellido_coordinador" placeholder="Apellido" class="form-control" onKeyPress="return valida(event,this,0,100)"
						 onBlur="valida2(this,0,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Teléfono Local</label>
						<input type="text" name="tlf_local" data-inputmask="'mask' : '(999) 999-9999'" id="tlf_local" placeholder="Teléfono Local" class="form-control" onKeyPress="return valida(event,this,10,100)"
						 onBlur="valida2(this,10,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>Teléfono Móvil</label>
						<input type="text" name="tlf_movil" id="tlf_movil" placeholder="Teléfono Móvil" data-inputmask="'mask' : '(999) 999-9999'" class="form-control" onKeyPress="return valida(event,this,10,100)"
						 onBlur="valida2(this,10,100)">
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 form-group">
						<label>E-Mail</label>
						<input type="email" name="email" id="email" placeholder="Correo Electrónico" class="form-control" onKeyPress="return valida(event,this,7,100)"
						 onBlur="valida2(this,7,100)">
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<br>
				<div class="text-center">
					<button id="basicoSubmit" class="btn btn-dark" value="Guardar" name="boton" disabled>Guardar</button>
				</div>
			</div>
		</div>
		<!-- /formulario para guardar persona -->
	</div>
		<?= form_close()?>
</div>
