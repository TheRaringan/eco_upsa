<!-- formulario para guardar capacidad -->
<?=form_open('capacidad_vocacion/insertar_capacidad_vocacion', array('name' => 'formulario_capacidad', 'id' => 'formulario_capacidad'))?>
	<div class="right_col" role="main">
		<div class="">
			<!--//boton buscar-->
			<div class="clearfix"></div>
			<div class="row">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							<?=$sub_titulo?>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							<label>Nombre de la UPSA</label>
							<input type="text" name="nombre_upsa" id="nombre_upsa" placeholder="Nombre de la UPSA" disabled class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>" onKeyPress="return valida(event,this,0,100)"
							 onBlur="valida2(this,0,100)">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Superficie Estabulada</label>
							<input type="text" name="superficie_estabulada" id="superficie_estabulada" placeholder="Superficie Estabulada" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();"
						 onKeyPress="return valida(event,this,13,9)" onBlur="valida2(this,13,9)">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Superficie Operativa Agrícola</label>
							<input type="text" name="superficie_operativa_agricola" id="superficie_operativa_agricola" placeholder="Superficie Operativa Agrícola"
							 class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,13,9)"
							 onBlur="valida2(this,13,9)">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Superficie Operativa Pecuaria</label>
							<input type="text" name="superficie_operativa_pecuaria" id="superficie_operativa_pecuaria" placeholder="Superficie Operativa Pecuaria"
							 class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,13,9)"
							 onBlur="valida2(this,13,9)">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Superficie Reserva Natural</label>
							<input type="text" name="superficie_reserva_natural" id="superficie_reserva_natural" placeholder="Superficie Reserva Natural"
							 class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,13,9)"
							 onBlur="valida2(this,13,9)" onchange="Suma(this.value); Suma1(this.value)">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Superficie Aprovechable Agrícola</label>
							<input type="text" name="superficie_aprovechable_agricola" id="superficie_aprovechable_agricola" placeholder="Superficie Aprovechable Agrícola"
							 class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,13,9)"
							 onBlur="valida2(this,13,9)" onchange="Suma(this.value); Suma1(this.value)">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Superficie Aprovechable Pecuaria</label>
							<input type="text" name="superficie_aprovechable_pecuaria" id="superficie_aprovechable_pecuaria" placeholder="Superficie Aprovechable Pecuaria"
							 class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" onKeyPress="return valida(event,this,13,9)"
							 onBlur="valida2(this,13,9)" onchange="Suma(this.value); Suma1(this.value)">
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Superficie Aprovechable Total</label>
							<input type="text" name="superficie_aprovechable_total" id="superficie_aprovechable_total" placeholder="Superficie Aprovechable Total"
							 class="form-control" disabled>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Total Hectáreas</label>
							<input type="text" name="total_hectareas" id="total_hectareas" placeholder="Total de Hectáreas" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();"
							 onKeyPress="return valida(event,this,13,9)" onBlur="valida2(this,13,9)" disabled>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Sistema de Riego</label>
							<select name="id_sistema_riego" id="id_sistema_riego" class="select2_single form-control col-md-4" tabindex="-1">
								<option value='0'> Sistema de Riego </option>
								<?php foreach ($sistemas_riego as $sistema_riego): ?>
								<option value='<?=$sistema_riego['id_sistema_riego']?>'>
									<?=$sistema_riego['nombre']?>
								</option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
				</div>
				<div class="x_panel">
					<div class="x_title">
						<div>
							<h2>Vocación</h2>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
							<div class="table-responsive">
								<table class="table table-hover table-bordered">
									<thead>
										<tr>
											<th>USO</th>
											<th>CLASE</th>
											<th>RUBROS</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td rowspan="4" style="vertical-align : middle;text-align:center;">AGRICOLA</td>
											<td>CLASE I</td>
											<td>Hortalizas, Leguminosas.</td>
											<td><input value="1" id="vocacion1" type="checkbox" class="vocacion" name="clase[]"/></td>
										</tr>
										<tr>
											<td>CLASE II</td>
											<td>
												Hortalizas, Leguminosas, Cereales, Musáceas, Raíces y Tubérculos, Plantaciones Tropicales conservacionistas (Café y Cacao).
											</td>
											<td>
												<input value="2"  id="vocacion2" type="checkbox" class="vocacion" name="clase[]"/>
											</td>
										</tr>
										<tr>
											<td>CLASE III</td>
											<td>
												Frutales, Cereales, Oleaginosas, Raíces y Tubérculos, Plantaciones Tropicales conservacionistas (Café y Cacao).
											</td>
											<td>
												<input value="3"  id="vocacion3" type="checkbox" class="vocacion" name="clase[]" />
											</td>
										</tr>
										<tr>
											<td>CLASE IV</td>
											<td>Raíces y Tubérculos, Frutales, Plantaciones Tropicales.</td>
											<td>
												<input value="4"  id="vocacion4" type="checkbox" class="vocacion" name="clase[]" />
											</td>
										</tr>
										<tr>
											<td rowspan="2" style="vertical-align : middle;text-align:center;">PECUARIO</td>
											<td>CLASE V</td>
											<td>
												Ganado Vacuno: Leche, Doble Propósito (Leche, Carne), Cría, Ganado Bufalino,  Caprino, Ovino, Porcino, Avícola, Especie de Fauna Silvestre.
											</td>
											<td>
												<input value="5"  id="vocacion5" type="checkbox" class="vocacion" name="clase[]" />
											</td>
										</tr>
										<tr>
											<td>CLASE VI</td>
											<td>
												Ganado Vacuno: Leche, Doble Propósito (Leche, Carne), Cría, Ganado Bufalino,  Caprino, Ovino, Porcino, Avícola, Especie de Fauna Silvestre.
											</td>
											<td>
												<input value="6"  id="vocacion6" type="checkbox" class="vocacion" name="clase[]" />
											</td>
										</tr>
										<tr>
											<td rowspan="2" style="vertical-align : middle;text-align:center;">FORESTAL</td>
											<td>CLASE VII</td>
											<td>Agroforestal: Plantaciones Forestales y Vida Silvestre</td>
											<td>
												<input value="7"  id="vocacion7" type="checkbox" class="vocacion" name="clase[]" />
											</td>
										</tr>
										<tr>
											<td>CLASE VIII</td>
											<td>Agroforestal: Plantaciones Forestales</td>
											<td>
												<input value="8"  id="vocacion8" type="checkbox" class="vocacion" name="clase[]" />
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<script type="text/javascript">
						function Suma(valor) {
							var TotalSuma = 0;
							//valor1 = ("superficie_aprovechable_agricola");
							var valor1 = $("#superficie_aprovechable_agricola").val();
							var valor2 = $("#superficie_aprovechable_pecuaria").val();
							TotalSuma = (TotalSuma == null || TotalSuma == undefined || TotalSuma == "") ? 0 : TotalSuma;
							TotalSuma5 = (parseInt(valor1) + parseInt(valor2));
							 $("#superficie_aprovechable_total").val(TotalSuma5);
							}

							function Suma1(valor) {
								var TotalSuma = 0;
								//valor1 = ("superficie_aprovechable_agricola");
								var valor1 = $("#superficie_reserva_natural").val();
								var valor2 =$("#superficie_aprovechable_agricola").val();
								var valor3 = $("#superficie_aprovechable_pecuaria").val();
								TotalSuma = (TotalSuma == null || TotalSuma == undefined || TotalSuma == "") ? 0 : TotalSuma;
								TotalSuma1 = (parseInt(valor1) + parseInt(valor2)+parseInt(valor3));
								console.log(TotalSuma1);
								 $("#total_hectareas").val(TotalSuma1);
								}
						</script>
						<div class="row text-center">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<br>
								<button type="submit" class="btn btn-dark" id="guardar_capacidad" name="guardar_capacidad" value="Guardar" disabled>
								Guardar
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

<?=form_close()?>
