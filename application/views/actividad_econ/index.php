<!-- page content -->
<div class="right_col" role="main">
	<form action="actividad_economica/insertar_actividad" method="POST" data-parsley-validate class="form-horizontal form-label-left">
		<div class="x_panel">
			<div class="x_title">
				<div>
					<h3>
						<!--<?=$title?>-->
						Actividad Economica
					</h3>
				</div>
				<div class="row text-right">
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Actividades Registradas</button>
				</div>
			</div>
			<div class="x_content">
				<div class="form-group">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							<label>UPSA</label>
							<input name="id_upsa" type="text" id="id_upsa" class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>" disabled>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<label>Tipo de Actividad Economica</label>
							<select id="a1" class="form-control select2_single">
								<option value="0">SELECCIONE</option>
								<?php foreach($tipos_actividades as $tipo_actividad):?>
								<option value="<?=$tipo_actividad['id_tipo_actividad_econ']?>">
									<?=$tipo_actividad['nombre']?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<label>Actividad Economica</label>
							<select id="a2" class="form-control select2_single">
								<option value="0">SELECCIONE</option>
							</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<label>RUBRO</label>
							<select id="a3" class="form-control select2_single">
								<option value="0">SELECCIONE</option>
							</select>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<label>UNIDAD DE MEDIDA</label>
							<select id="unidad_medida" class="form-control select2_single">
								<option value="0">--SELECCIONE--</option>
								<?php foreach($unidades_medidas as $unidad_medida):?>
								<option value="<?=$unidad_medida['id_unidad_medida']?>">
									<?=$unidad_medida['nombre']?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
						<!-- <div class="col-md-4 col-sm-12 col-xs-12">
							<label>SUB-RUBRO</label>
							<select id="a4" class="form-control select2_single">
								<option value="0">SELECCIONE</option>
							</select>
						</div> -->
					</div>
					<br>
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<label>PRODUCCION MENSUAL</label>
							<input id="produccion_mensual" type="text" class="form-control">
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<label>ESTADO DE OPERACIÓN</label>
							<select id="estado_operacion" class="form-control select2_single">
								<option value="0">--SELECCIONE--</option>
								<?php foreach($estados_operacion as $estado_operacion):?>
								<option value="<?=$estado_operacion['id_estado_operacion']?>">
									<?=$estado_operacion['nombre']?>
								</option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row text-center">
					<button type="button" id="actElement" class="btn btn-primary btn-circle waves-effect waves-circle waves-float ">
						<i class="fa fa-lg fa-plus"></i>
					</button>
				</div>
				<div class="ln_solid"></div>
				<div class="table-responsive">
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th>TIPO</th>
								<th>ACTIVIDAD</th>
								<th>RUBRO</th>
								<th>UNIDAD</th>
								<th>PRODUCCIÓN</th>
								<th>OPERACIÓN</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="actTable">
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal fade" id="myModal" role="dialog">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Actividades Economicas Registradas</h4>
				  </div>
				  <div class="modal-body">
					<p>A continuación se muestran las Actividades Economicas que ya han sido cargados por usted.</p>
					<p>
						<label>UPSA</label>
						<input name="id_upsa" type="text" id="id_upsa" class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>" disabled>
					</p>
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th>TIPO</th>
									<th>ACTIVIDAD</th>
									<th>RUBRO</th>
									<th>UNIDAD</th>
									<th>PRODUCCIÓN</th>
									<th>Est. Operacion</th>
								</tr>
							</thead>
							<tbody>
								<?php if($listado_act_prod != FALSE):?>
								<?php foreach ($listado_act_prod as $row) : ?>
								<tr>
									<td><?=$row[1]?></td>
									<td><?=$row[2]?></td>
									<td><?=$row[3]?></td>
									<td><?=$row[4]?></td>
									<td><?=$row[5]?></td>
									<td><?=$row[6]?></td>
								</tr>
								<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				  </div>


				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				</div>

			  </div>
			</div>

			<!-- /page content -->

		</div>
		<div class="x_panel">
			<div id="switches">
				<div class="x_title">
					<div class="text-center">
						<h3>Debilidades</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-sm-2">
							<label>LABORAL</label>
							<input value="2" type="checkbox" class="js-switch" name="id_laboral" />
						</div>
						<div class="col-sm-2">
							<label>ELECTRICIDAD</label>
							<input value="2" type="checkbox" class="js-switch" name="id_electricidad" />
						</div>
						<div class="col-sm-2">
							<label>MECANICO</label>
							<input value="2" type="checkbox" class="js-switch" name="id_mecanico" />
						</div>
						<div class="col-sm-2">
							<label>SEGURIDAD</label>
							<input value="2" type="checkbox" class="js-switch" name="id_seguridad" />
						</div>
						<div class="col-sm-2">
							<label>CLIMATICO</label>
							<input value="2" type="checkbox" class="js-switch" name="id_climatico" />
						</div>
						<div class="col-sm-2">
							<label>MATERIA PRIMA</label>
							<input value="2" type="checkbox" class="js-switch" name="id_materia_prima" />
						</div>
					</div>
				</div>
			</div>
			<div class="ln_solid"></div>
			<label for="">Observaciones</label>
			<textarea class="form-control" name="observaciones" id="" cols="20" rows="5" placeholder="Ingrese Observacion"></textarea>
			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="text-center">
						<button id="actSubmit" type="submit" class="btn btn-dark">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
