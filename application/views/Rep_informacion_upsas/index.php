<div class="right_col" role="main">
	<div class="">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Upsas Registradas y Coordinador</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="datatable-keytable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>UPSA</th>
								<th>Coordinador</th>
								<th>Cedula</th>
								<th>Coordinador</th>
								<th>Telf. Coordinador</th>
							</tr>
						</thead>
						<tbody>
							<?php if($Listado != FALSE):?>
							<?php foreach ($Listado as $row) : ?>
					       		<tr>
						            <td><?=$row[0]?></td>
						            <td><?=$row[1]?></td>
						            <td><?=$row[2]?></td>
						            <td><?=$row[3]?></td>
						            <td><?=$row[4]?></td>
				        		</tr>
				    		<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
