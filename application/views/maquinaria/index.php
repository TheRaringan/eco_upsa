<div class="right_col" role="main">
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>
						<?=$titulo?>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<form class="form-horizontal form-label-left" >
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								<label>UPSA</label>
								<input name="id_upsa" type="text" id="id_upsa" class="form-control" value="<?=$this->session->userdata('nombre_upsa')?>"
								  disabled>
							</div>
						</div>
						<!---->
						<div class=" col-md-3 col-sm-12 col-xs-12form-group">
                        	<label>Fecha de adquisición </label>
                          	<input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                      	</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Operatividad</label>
							<select name="id_operatividad" id="id_operatividad" class="select2_single form-control">
								<option value="0">SELECCIONE</option>
								<option value="1">Operativo</option>
								<option value="2">Inoperativo</option>
								<option value="3">S/I</option>
							</select>
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Descripción</label>
							<input name="descripcion" type="text" id="descripcion" placeholder="Descripción" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Tipología</label>
							<select name="tipologia" id="tipologia" class="select2_single form-control">
								<option value="0">SELECCIONE</option>
								<option value="1">Implemento</option>
								<option value="2">Maquinaria</option>
								<option value="3">Vehículo</option>
								<option value="4">S/I</option>
							</select>
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Tipo</label>
							<select name="tipo" id="tipo" class="select2_single form-control">
								<option value="0">SELECCIONE</option>
								<option value="1">Asperjadora</option>
								<option value="2">Camion 750</option>
								<option value="3">Camioneta</option>
								<option value="4">Pulverizador</option>
							</select>
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Subclase</label>
							<select name="subclase" id="subclase" class="select2_single form-control">
								<option value="0">SELECCIONE</option>
								<option value="1">Aereo</option>
								<option value="2">Doble</option>
								<option value="3">Transporte</option>
								<option value="4">S/I</option>
							</select>
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Marca</label>
							<select name="marca" id="marca" class="select2_single form-control">
								<option value="0">SELECCIONE</option>
								<option value="1">2x3 M</option>
								<option value="2">Chevrolet</option>
								<option value="3">IVECO</option>
								<option value="4">S/I</option>
							</select>
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Modelo</label>
							<select name="modelo" id="modelo" class="select2_single form-control">
								<option value="0">SELECCIONE</option>
								<option value="1">C 101</option>
								<option value="2">Colorado</option>
								<option value="3">MF4299</option>
								<option value="4">S/I</option>
							</select>
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Año</label>
							<input name="anio" type="text" id="anio" placeholder="Ej:2012" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Código externo</label>
							<input name="cod_externo" type="text" id="cod_externo" placeholder="Código Externo" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Serial de la carroceria</label>
							<input name="serial_carroceria" type="text" id="serial_carroceria" placeholder="Serial de la Carroceria" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-3 col-sm-12 col-xs-12 form-group">
							<label>Serial del Motor</label>
							<input name="serial_motor" type="text" id="serial_motor" placeholder="Serial Del Motor" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Horas de trabajo</label>
							<input name="horas_trabajo" type="text" id="horas_trabajo" placeholder="Horas de trabajo" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Mantenimiento</label>
							<input name="mantenimiento" type="text" id="mantenimiento" placeholder="Mantenimiento" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
						<!---->
						<div class="col-md-4 col-sm-12 col-xs-12 form-group">
							<label>Observaciones</label>
							<input name="observaciones" type="text" id="observaciones" placeholder="Observaciones" class="form-control" onKeyPress="return valida(event,this,2,100)"
							  onBlur="valida2(this,2,100)">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
