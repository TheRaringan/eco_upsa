<body class="nav-md">
	<div class="container body">
		<div class="main_container">
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Upsas Registradas en Venezuela</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <!-- Large modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target=".bs-example-modal-lg">Agregar Coords de Upsas</button>

                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <form action="" method="POST">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Agregar Coordenadas</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                              <input type="text" name="longitud"placeholder="Longitud" class="form-control">
                            </div>
                            <div  class="col-md-4 col-sm-12 col-xs-12 form-group">
                              <input type="text" name="latitud" placeholder="Latitud" class="form-control">
                            </div>
                            <div  class="col-md-4 col-sm-12 col-xs-12 form-group">
                              <select name="upsa" class="select2_single upsa_select form-control" tabindex="-1" style="width: 250px;">
                                <option></option>
                                <?php foreach($upsas as $upsa) : ?>
                                <option value="<?php echo $upsa['id_upsa']; ?>"><?php echo $upsa['nombre']; ?></option>
                                <?php endforeach; ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div id="map" style="height:590px; width:100%;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
            <!-- /page content -->
