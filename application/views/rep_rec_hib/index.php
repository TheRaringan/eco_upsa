<div class="right_col" role="main">
	<div class="">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Reporte de Actividad Economica</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<table id="datatable-keytable" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Estado</th>
									<th>Nombre UPSA</th>
									<th>Tipo Recurso Hidrico</th>
									<th>Recurso Hidrico</th>
								</tr>
							</thead>
							<tbody>
								<?php if($listado_rec_hib != FALSE):?>
								<?php foreach ($listado_rec_hib as $row) : ?>
								<tr>
									<td><?=$row[0]?></td>
									<td><?=$row[1]?></td>
									<td><?=$row[2]?></td>
									<td><?=$row[3]?></td>
								</tr>
								<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--<div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tipo Recurso Hidrico</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="echart_donut" style="height:350px;"></div>
                  </div>
                </div>
              </div>

			<div class="col-md-6 col-sm-12 col-xs-12">
			   <div class="x_panel">
				 <div class="x_title">
				   <h2>Recurso Hidrico</h2>
				   <div class="clearfix"></div>
				 </div>
				 <div class="x_content">
				   <div id="echart_mini_pie" style="height:350px;"></div>
				 </div>
			   </div>
		   </div>-->
		</div>
	</div>
</div>

<script>
  var theme = {
	  color: [
		  '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
		  '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
	  ],

	  title: {
		  itemGap: 8,
		  textStyle: {
			  fontWeight: 'normal',
			  color: '#408829'
		  }
	  },

	  dataRange: {
		  color: ['#1f610a', '#97b58d']
	  },

	  toolbox: {
		  color: ['#408829', '#408829', '#408829', '#408829']
	  },

	  tooltip: {
		  backgroundColor: 'rgba(0,0,0,0.5)',
		  axisPointer: {
			  type: 'line',
			  lineStyle: {
				  color: '#408829',
				  type: 'dashed'
			  },
			  crossStyle: {
				  color: '#408829'
			  },
			  shadowStyle: {
				  color: 'rgba(200,200,200,0.3)'
			  }
		  }
	  },

	  dataZoom: {
		  dataBackgroundColor: '#eee',
		  fillerColor: 'rgba(64,136,41,0.2)',
		  handleColor: '#408829'
	  },
	  grid: {
		  borderWidth: 0
	  },

	  categoryAxis: {
		  axisLine: {
			  lineStyle: {
				  color: '#408829'
			  }
		  },
		  splitLine: {
			  lineStyle: {
				  color: ['#eee']
			  }
		  }
	  },

	  valueAxis: {
		  axisLine: {
			  lineStyle: {
				  color: '#408829'
			  }
		  },
		  splitArea: {
			  show: true,
			  areaStyle: {
				  color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
			  }
		  },
		  splitLine: {
			  lineStyle: {
				  color: ['#eee']
			  }
		  }
	  },
	  timeline: {
		  lineStyle: {
			  color: '#408829'
		  },
		  controlStyle: {
			  normal: {color: '#408829'},
			  emphasis: {color: '#408829'}
		  }
	  },

	  k: {
		  itemStyle: {
			  normal: {
				  color: '#68a54a',
				  color0: '#a9cba2',
				  lineStyle: {
					  width: 1,
					  color: '#408829',
					  color0: '#86b379'
				  }
			  }
		  }
	  },
	  map: {
		  itemStyle: {
			  normal: {
				  areaStyle: {
					  color: '#ddd'
				  },
				  label: {
					  textStyle: {
						  color: '#c12e34'
					  }
				  }
			  },
			  emphasis: {
				  areaStyle: {
					  color: '#99d2dd'
				  },
				  label: {
					  textStyle: {
						  color: '#c12e34'
					  }
				  }
			  }
		  }
	  },
	  force: {
		  itemStyle: {
			  normal: {
				  linkStyle: {
					  strokeColor: '#408829'
				  }
			  }
		  }
	  },
	  chord: {
		  padding: 4,
		  itemStyle: {
			  normal: {
				  lineStyle: {
					  width: 1,
					  color: 'rgba(128, 128, 128, 0.5)'
				  },
				  chordStyle: {
					  lineStyle: {
						  width: 1,
						  color: 'rgba(128, 128, 128, 0.5)'
					  }
				  }
			  },
			  emphasis: {
				  lineStyle: {
					  width: 1,
					  color: 'rgba(128, 128, 128, 0.5)'
				  },
				  chordStyle: {
					  lineStyle: {
						  width: 1,
						  color: 'rgba(128, 128, 128, 0.5)'
					  }
				  }
			  }
		  }
	  },
	  gauge: {
		  startAngle: 225,
		  endAngle: -45,
		  axisLine: {
			  show: true,
			  lineStyle: {
				  color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
				  width: 8
			  }
		  },
		  axisTick: {
			  splitNumber: 10,
			  length: 12,
			  lineStyle: {
				  color: 'auto'
			  }
		  },
		  axisLabel: {
			  textStyle: {
				  color: 'auto'
			  }
		  },
		  splitLine: {
			  length: 18,
			  lineStyle: {
				  color: 'auto'
			  }
		  },
		  pointer: {
			  length: '90%',
			  color: 'auto'
		  },
		  title: {
			  textStyle: {
				  color: '#333'
			  }
		  },
		  detail: {
			  textStyle: {
				  color: 'auto'
			  }
		  }
	  },
	  textStyle: {
		  fontFamily: 'Arial, Verdana, sans-serif'
	  }
  };

  var dataStyle = {
	normal: {
	  label: {
		show: false
	  },
	  labelLine: {
		show: false
	  }
	}
  };

  var placeHolderStyle = {
	normal: {
	  color: 'rgba(0,0,0,0)',
	  label: {
		show: false
	  },
	  labelLine: {
		show: false
	  }
	},
	emphasis: {
	  color: 'rgba(0,0,0,0)'
	}
  };

  var echartMiniPie = echarts.init(document.getElementById('echart_mini_pie'), theme);

       echartMiniPie .setOption({
         title: {
           text: '.',
           subtext: 'Datos de Prueba',
           sublink: 'http://e.weibo.com/1341556070/AhQXtjbqh',
           x: 'center',
           y: 'center',
           itemGap: 20,
           textStyle: {
             color: 'rgba(30,144,255,0.8)',
             fontFamily: '微软雅黑',
             fontSize: 35,
             fontWeight: 'bolder'
           }
         },
         tooltip: {
           show: true,
           formatter: "{a} <br/>{b} : {c} ({d}%)"
         },
         legend: {
           orient: 'vertical',
           x: 270,
           y: 45,
           itemGap: 12,
           data: ['Pozos', 'Lagunas', 'Rios','Quebradas'],
         },
         toolbox: {
           show: true,
           feature: {
             mark: {
               show: true
             },
             dataView: {
               show: true,
               title: "Text View",
               lang: [
                 "Text View",
                 "Close",
                 "Refresh",
               ],
               readOnly: false
             },
             restore: {
               show: true,
               title: "Restore"
             },
             saveAsImage: {
               show: true,
               title: "Save Image"
             }
           }
         },
         series: [{
           name: '1',
           type: 'pie',
           clockWise: false,
           radius: [130, 150],
           itemStyle: dataStyle,
           data: [{
             value: 68,
             name: 'Pozos'
           }, {
             value: 32,
             name: 'invisible',
             itemStyle: placeHolderStyle
           }]
         }, {
           name: '2',
           type: 'pie',
           clockWise: false,
           radius: [110, 130],
           itemStyle: dataStyle,
           data: [{
             value: 29,
             name: 'Lagunas'
           }, {
             value: 71,
             name: 'invisible',
             itemStyle: placeHolderStyle
           }]
         }, {
           name: '3',
           type: 'pie',
           clockWise: false,
           radius: [90, 110],
           itemStyle: dataStyle,
           data: [{
             value: 50,
             name: 'Rios'
           }, {
             value: 97,
             name: 'invisible',
             itemStyle: placeHolderStyle
           }]
	   	},{
		  name: '3',
		  type: 'pie',
		  clockWise: false,
		  radius: [30, 90],
		  itemStyle: dataStyle,
		  data: [{
			value: 20,
			name: 'Quebradas'
		  }, {
			value: 97,
			name: 'invisible',
			itemStyle: placeHolderStyle
		  }]
	   }]
       });

	   var echartDonut = echarts.init(document.getElementById('echart_donut'), theme);

      echartDonut.setOption({

		  title: {
            //text: 'Echart Pyramid Graph',
            subtext: 'Datos de Prueba'
          },
        tooltip: {
          trigger: 'item',
          formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        calculable: true,
        legend: {
          x: 'center',
          y: 'bottom',
          data: ['Direct Access', 'E-mail Marketing']
        },
        toolbox: {
          show: true,
          feature: {
            magicType: {
              show: true,
              type: ['pie', 'funnel'],
              option: {
                funnel: {
                  x: '25%',
                  width: '50%',
                  funnelAlign: 'center',
                  max: 1548
                }
              }
            },
            restore: {
              show: true,
              title: "Restore"
            },
            saveAsImage: {
              show: true,
              title: "Save Image"
            }
          }
        },
        series: [{
          name: 'Access to the resource',
          type: 'pie',
          radius: ['35%', '55%'],
          itemStyle: {
            normal: {
              label: {
                show: true
              },
              labelLine: {
                show: true
              }
            },
            emphasis: {
              label: {
                show: true,
                position: 'center',
                textStyle: {
                  fontSize: '14',
                  fontWeight: 'normal'
                }
              }
            }
          },
          data: [{
            value: 335,
            name: 'Permanentes'
          }, {
            value: 310,
            name: 'Intermitentes'
          }]
        }]
      });

</script>
