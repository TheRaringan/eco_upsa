﻿CREATE VIEW public.ultima_entrega
AS SELECT ultima_entrega.* FROM (
	SELECT nota_entrega.*, 
	rank() OVER (
		PARTITION BY id_ca
		ORDER BY fecha DESC
	)
	FROM nota_entrega
) ultima_entrega WHERE rank = 1;

SELECT fecha FROM ultima_entrega
