/* form = document.querySelector('.confirm-form');

if(form){
	form.addEventListener('submit',confirmForm);
}

function confirmForm(){
	return confirm('¿Esta seguro que la información introducida es correcta?');
} */

$(document).ready(function(){
	$('#mySubmit').click(function(e){
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
            title: "Confirmación",
            text: '¿La información que introdujo es correcta?', 
            icon: "warning",
            buttons: {
				cancel: "Cancelar",
				aceptar: true,
			},
			dangerMode: true,
    })
    .then((confirm) => {
      if (confirm) {
        swal("Confirmado!", {
        	icon: "success",
				})
				.then((submit)=>{
					if(submit){
						form.submit();
					}
				})
      } else {
        swal("Ha cancelado el envio del formulario!",{
					icon: "error",
				});
      }
    });
	});

	var check_alert = $('#receipt_exists');

	switch(check_alert.val()){
		case '1': {
			swal('La factura indicada ya existe','','error');
			check_alert.remove();
			break;
		}
	}
});
