const domain = document.querySelector('#domain');
function initMap(){

  const options = {
    zoom:6,
    center:{lat:8.0000000,lng:-66.0000000}
  }

  const map = new google.maps.Map(document.getElementById('map'), options);

  async function get(url){
    const response = await fetch(url);
    const resData = response.json();
    return resData;
  }

  get(base_url+'index.php/upsas_ajax/get_upsas_coords')
    .then((data) => {
      const markers = [];

      for(let i in data){
        markers.push({
          coords:{lat:parseFloat(data[i].latitud),lng:parseFloat(data[i].longitud)},
          content:`<h3>${data[i].nombre}</h3>`
        },)
      }

      for(let i in markers){
        addMarker(markers[i]);
      }

      // Add Marker
      function addMarker(props){
        const marker = new google.maps.Marker({
          position:props.coords,
          map:map,
          label: 'U'
          //icon:props.iconImage
        });

        if(props.iconImage){
          // Set icon image
          marker.setIcon(props.iconImage);
        }

        // Check content
        if(props.content){
          var infoWindow = new google.maps.InfoWindow({
            content:props.content
          });

          marker.addListener('click', function(){
            infoWindow.open(map, marker);
          });
        }
      }
    })
    .catch((error) => console.log(error));
}
