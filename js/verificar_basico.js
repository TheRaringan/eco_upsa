$(document).ready(function () {
  if ($('#formulario_basico').length && id_upsa != '') {
    $.ajax({
      url: base_url + 'consultas_ajax/get_info_upsa',
      method: 'post',
      dataType: 'json',
      data: {
        this_upsa: id_upsa
      },
      success: function (response) {
        console.log(response);
        $('#nombre_upsa').prop('disabled', true).val(response['nombre_upsa']);
        $('#cod_insai').prop('disabled', true).val(response['cod_insai']);
        $('#cod_sunagro').prop('disabled', true).val(response['cod_sunagro']);
        $('#id_agropatria').prop('disabled', true).val(response['id_agropatria']).trigger('change');
        $('#id_tenencia_tierra').prop('disabled', true).val(response['id_tenencia_tierra']).trigger('change');
        $('#id_relacion_corpo').prop('disabled', true).val(response['id_relacion_corpo']).trigger('change');
        $('#cedula').prop('disabled', true).val(response['cedula']);
        $('#nombre_coordinador').prop('disabled', true).val(response['nombre_coordinador']);
        $('#apellido_coordinador').prop('disabled', true).val(response['apellido_coordinador']);
        $('#tlf_movil').prop('disabled', true).val(response['tlf_movil']);
        $('#tlf_local').prop('disabled', true).val(response['tlf_local']);
        $('#email').prop('disabled', true).val(response['email']);
      }
    })
  }

  if ($('#formulario_ubicacion_registrado').length && id_upsa != '') {
    $.ajax({
      url: base_url + 'consultas_ajax/get_info_ubicacion',
      method: 'post',
      dataType: 'json',
      data: {
        this_upsa: id_upsa
      },
      success: function (response) {
        console.log(response);
        $('#id_region').prop('disabled', true).val(response['region']);
        $('#id_estado').prop('disabled', true).val(response['estado']);
        $('#id_municipio').prop('disabled', true).val(response['municipio']);
        $('#id_parroquia').prop('disabled', true).val(response['parroquia']);
        $('#cod_upsa').prop('disabled', true).val(response['cod_upsa']);
        $('#direccion').prop('disabled', true).val(response['direccion']);
        $('#lindero_norte').prop('disabled', true).val(response['lindero_norte']);
        $('#lindero_sur').prop('disabled', true).val(response['lindero_sur']);
        $('#lindero_este').prop('disabled', true).val(response['lindero_este']);
        $('#lindero_oeste').prop('disabled', true).val(response['lindero_oeste']);
      }
    })
  }

  if ($('#formulario_capacidad').length && id_upsa != '') {
    $.ajax({
      url: base_url + 'consultas_ajax/get_info_capacidad',
      method: 'post',
      dataType: 'json',
      data: {
        this_upsa: id_upsa
      },
      success: function (response) {
        console.log(response);
        if (response != false) {
          $('#superficie_estabulada').prop('disabled', true).val(response['superficie_estabulada']);
          $('#superficie_operativa_agricola').prop('disabled', true).val(response['superficie_operativa_agricola']);
          $('#superficie_operativa_pecuaria').prop('disabled', true).val(response['superficie_operativa_pecuaria']);
          $('#superficie_reserva_natural').prop('disabled', true).val(response['superficie_reserva_natural']);
          $('#superficie_aprovechable_agricola').prop('disabled', true).val(response['superficie_aprovechable_agricola']);
          $('#superficie_aprovechable_pecuaria').prop('disabled', true).val(response['superficie_aprovechable_pecuaria']);
          $('#superficie_aprovechable_total').prop('disabled', true).val(response['superficie_aprovechable_total']);
          $('#total_hectareas').prop('disabled', true).val(response['total_hectareas']);
          $('#id_sistema_riego').prop('disabled', true).val(response['id_sistema_riego']).trigger('change');
        }
      }
    })
  }

  if ($('#formulario_capacidad').length && id_upsa != '') {
    $.ajax({
      url: base_url + 'consultas_ajax/get_info_vocacion',
      method: 'post',
      dataType: 'json',
      data: {
        this_upsa: id_upsa
      },
      success: function (response) {
        if (response != false) {

          $.each($('.vocacion'),function(){
           $(this).prop('disabled',true);
          })

          console.log($('.vocacion').iCheck('update')[0].checked);

          $.each(response, function (index, data) {
            console.log(data['id_vocacion']);
            if ($("#vocacion" + data['id_vocacion']).val() == data['id_vocacion']) {
              $("#vocacion"+data['id_vocacion']).iCheck('check');
            }
          })

        }
      }
    })
  }

});