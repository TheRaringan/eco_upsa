$(document).ready(function() {
  console.log($('#myPnotify'));
  if ($("#myPnotify")) {
    pnoti = $('#myPnotify').val();
    console.log(pnoti);
    switch (pnoti) {
      case "logout":
        console.log('fuaaaaaa');
        pnotify = new PNotify({
          title: "Has Cerrado Sesión!",
          type: "error",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

      case "insert":
        pnotify = new PNotify({
          title: "Registro Exitoso!",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;

      case "login":
        pnotify = new PNotify({
          title: "Has iniciado Sesión!",
          type: "success",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;


 	case "incorrecto":
          pnotify = new PNotify({
            title: "Usuario o Contraseña Incorrecta!",
            type: "warning",
            styling: "bootstrap3"
          });
          setTimeout(() => {
            pnotify.remove();
            $('#myPnotify').remove();
          }, 3500);
          break;

      case "captcha":
        pnotify = new PNotify({
          title: "Por favor rellene el ReCaptcha antes de continuar",
          type: "warning",
          styling: "bootstrap3"
        });
        setTimeout(() => {
          pnotify.remove();
          $('#myPnotify').remove();
        }, 3500);
        break;
        case "exists":
          pnotify = new PNotify({
            title: "Registro ya Existe",
            type: "warning",
            styling: "bootstrap3"
          });
          setTimeout(() => {
            pnotify.remove();
            $('#myPnotify').remove();
          }, 3500);
          break;


        case "alert":
          pnotify = new PNotify({
            title: "Debe Ingresar una region",
            type: "warning",
            styling: "bootstrap3"
          });
          setTimeout(() => {
            pnotify.remove();
            $('#myPnotify').remove();
          }, 3500);
          break;
    }
  }else{
    console.log('pnoti not found');
  }
});
