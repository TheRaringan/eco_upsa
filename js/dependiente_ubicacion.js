		$(document).ready(function(){
		  //################################## INFO DE INSUMO ############################################
		  //Cambio en Tipo
			$('#id_region').change(function(){
					var id_region = $(this).val();

					// AJAX request
					 $.ajax({
							url:base_url+'ubicacion/get_estado',
							method: 'post',
							data: {this_id_region: id_region},
							dataType: 'json',
							success: function(response){

									// Remove options
									$('#id_estado').find('option').not(':first').remove();
									$('#id_municipio').find('option').not(':first').remove();
									$('#id_parroquia').find('option').not(':first').remove();
									// Add options
									$.each(response,function(index,data){
											$('#id_estado').append('<option value="'+data['id_estado']+'">'+data['descripcion']+'</option>');
									});
							}
					});
			});
		});


		$('#id_estado').change(function(){
		        var id_estado = $(this).val();

		        // AJAX request
		         $.ajax({
		            url:base_url+'ubicacion/get_municipio',
		            method: 'post',
		            data: {this_id_estado: id_estado},
		            dataType: 'json',
		            success: function(response){

		                // Remove options
										$('#id_municipio').find('option').not(':first').remove();
										$('#id_parroquia').find('option').not(':first').remove();
		                // Add options
		                $.each(response,function(index,data){
		                    $('#id_municipio').append('<option value="'+data['id_municipio']+'">'+data['descripcion']+'</option>');
		                });
		            }
		        });
		    });

				$('#id_municipio').change(function(){
					var id_municipio = $(this).val();
					// AJAX request
					 $.ajax({
							url:base_url+'ubicacion/get_parroquia',
							method: 'post',
							data: {this_id_municipio: id_municipio},
							dataType: 'json',
							success: function(response){

									// Remove options
									$('#id_parroquia').find('option').not(':first').remove();
									// Add options
									$.each(response,function(index,data){
											$('#id_parroquia').append('<option value="'+data['id_parroquia']+'">'+data['descripcion']+'</option>');
									});
							}
					});
			});
