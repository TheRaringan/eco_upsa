function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
};

$(document).ready(function(){

  $('.parse-float').change(function(){

    if($(this).val() < 999999.99){
      var number = numeral(round($(this).val(),1)).format('000000.00');
      $(this).val(number);
    }else{
      $(this).val('');
    }
  });
});