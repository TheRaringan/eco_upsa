const Uibtn = document.querySelector('#myElement');
const UiTable = document.querySelector('#myTable');
const UisTipo = document.querySelector('#s1');
const UisRecurso = document.querySelector('#s2');
const UiBtnSubmit = document.querySelector('#mySubmit');
var increment = 0;

document.addEventListener('DOMContentLoaded', () => {
  if(UiBtnSubmit)
  UiBtnSubmit.disabled = true;
});

function addEventHandler(){
  if(UisTipo.value == 0 || UisRecurso.value == 0){
    console.log('No paso');
  }else{
    if(UiBtnSubmit){
      UiBtnSubmit.disabled = false;
    }
    UisRecurso.options[0].selected;
    const tr = document.createElement('tr');
    increment = increment +1;
    tr.className='myTr';
    tr.innerHTML = `
      <td>${UisTipo.selectedOptions[0].text}<input type="text" name="tipo[]" id="ins-type-${increment}" hidden value="${UisTipo.value}"></td>
      <td>${UisRecurso.selectedOptions[0].text}<input type="text" name="recurso[]" id="ins-subtype-${increment}" hidden value="${UisRecurso.value} "></td>
      <td>
        <div class="row clearfix text-center">
          <a href="#" class="deleteBtn"><i class="fa fa-lg fa-trash"></i></a>
        </div>
      </td>
    `;
    UiTable.appendChild(tr);
    UisTipo.value = 0;
    UisTipo.dispatchEvent(new Event('change'));
  }
}


function deleteEventHandler(e){
  if(e.target.parentElement.classList.contains('deleteBtn')){
    e.target.parentElement.parentElement.parentElement.parentElement.remove();
    UisTipo.value = 0;
    UisTipo.dispatchEvent(new Event('change'));
    if(UiTable.childElementCount === 0){
      if(UiBtnSubmit){
        UiBtnSubmit.disabled = true;
      }

    }
	}
  e.preventDefault();
}

if(UiTable){
  Uibtn.addEventListener('click', addEventHandler);
  UiTable.addEventListener('click', deleteEventHandler);
}

