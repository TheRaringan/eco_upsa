//Validaciones para la vista de Registro de la UPSA
$(document).ready(function () {

  validate();
  $('#nombre_upsa, #cod_sunagro, #cod_insai, #id_agropatria, #id_tenencia_tierra, #id_relacion_corpo, #cedula, #nombre, #apellido, #email').change(validate);

  function validate(){
    if( ($('#nombre_upsa').val() =='') ||
        ($('#cod_sunagro').val() == '') ||
        ($('#cod_insai').val() == '') ||
        ($('#cedula').val()== '') ||
        ($('#nombre').val() == '') ||
        ($('#apellido').val() == '') ||
        ($('#tlf_movil').val() == '') ||
        ($('#email').val() == '') ||
        ($('#id_agropatria').val() == 0) ||
        ($('#id_tenencia_tierra').val() == 0) ||
        ($('#id_relacion_corpo').val() == 0 )){
      $('#basicoSubmit').prop('disabled', true);
    }else{
      $('#basicoSubmit').prop('disabled', false);
    }
  }

});
