$(document).ready(function () {

  validate();
   $('.vocacion').change(validate);

   function validate(){
     if($('.vocacion').is(':checked') && $('.vocacion').not(':disabled') && $('#id_sistema_riego').val() != 0){
       $('#guardar_capacidad').prop('disabled', false);
     }else{
       $('#guardar_capacidad').prop('disabled', true);
     }
   }
});
